<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package    mod
 * @subpackage moodecgrpmanagement
 * @copyright  2013 Université de Lausanne
 * @author     Nicolas Dunand <Nicolas.Dunand@unil.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @modifed by Martin Tazlari
 * @copyright 2016 Cyberlearn
 */

defined('MOODLE_INTERNAL') || die();

require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once ($CFG->dirroot.'/lib/formslib.php');
class mod_moodecgrpmanagement_mod_form extends moodleform_mod {

    function definition() {
        global $CFG,$USER, $moodecgrpmanagement_SHOWRESULTS, $moodecgrpmanagement_PUBLISH, $moodecgrpmanagement_DISPLAY, $DB, $COURSE, $PAGE;

        $mform    =& $this->_form;
        // -------------------------
        // General section
        // -------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('moodecgrpmanagementname', 'moodecgrpmanagement'), array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');

        $this->standard_intro_elements() ;

        $groups = array();

        $db_groups = $DB->get_records('groups', array('courseid' => $COURSE->id));

        if($db_groups) {
            foreach ($db_groups as $group) {
                $groups[$group->id] = new stdClass();
                $groups[$group->id]->name = $group->name;
                $groups[$group->id]->mentioned = false;
                $groups[$group->id]->id = $group->id;
			  	$groups[$group->id]->enrolmentkey = $group->enrolmentkey;
                $groups[$group->id]->creatorid = $USER->id;
            }
        }
        $db_groupings = $DB->get_records('groupings', array('courseid' => $COURSE->id));
        $groupings = array();
        if ($db_groupings) {
            foreach ($db_groupings as $grouping) {
                $groupings[$grouping->id] = new stdClass();
                $groupings[$grouping->id]->name = $grouping->name;
            }

            list($sqlin, $inparams) = $DB->get_in_or_equal(array_keys($groupings));
            $db_groupings_groups = $DB->get_records_select('groupings_groups', 'groupingid '.$sqlin, $inparams);

            foreach ($db_groupings_groups as $grouping_group_link) {
                $groupings[$grouping_group_link->groupingid]->linkedGroupsIDs[] =  $grouping_group_link->groupid;
            }
        }

        // -------------------------
        // Groups section
        // -------------------------
        $mform->addElement('header', 'groups', get_string('groupsheader', 'moodecgrpmanagement'));
        $mform->setExpanded('groups');
        $mform->addElement('selectyesno', 'groupcreationpossible', get_string('groupcreationpossible', 'moodecgrpmanagement'));

        $mform->addElement('selectyesno', 'privategroupspossible', get_string('privategroupspossible', 'moodecgrpmanagement'));
        $mform->disabledIf('privategroupspossible', 'groupcreationpossible', 'eq', 0);

        $mform->addElement('selectyesno', 'allowupdate', get_string("allowupdate", "moodecgrpmanagement"));

        $mform->addElement('selectyesno', 'multipleenrollmentspossible', get_string('multipleenrollmentspossible', 'moodecgrpmanagement'));

        $mform->addElement('selectyesno', 'limitmaxgroups', get_string('limitmaxgroups', 'moodecgrpmanagement'));

        $mform->addElement('text', 'maxgroups', get_string('maxgroups', 'moodecgrpmanagement'), array('size' => 6));
        $mform->setType('maxgroups', PARAM_INT);
        $mform->addRule('maxgroups', get_string('error'), 'numeric', 'extraruledata', 'client', false, false);
        $mform->setDefault('maxgroups', 0);
        $mform->disabledIf('maxgroups', 'limitmaxgroups', 'eq', 0);

        $serializedselectedgroupsValue = '';

        if (isset($this->_instance) && $this->_instance != '') {
            // this is presumably edit mode, try to fill in the data for javascript
            $cg = moodecgrpmanagement_get_moodecgrpmanagement($this->_instance);
            if(isset($cg->option)) {
                foreach ($cg->option as $optionID => $groupID) {
                    $serializedselectedgroupsValue .= ';' . $groupID;
                    $mform->setDefault('group_' . $groupID . '_limit', $cg->maxanswers[$optionID]);
                }
            }
        }

        $mform->addElement('selectyesno', 'limitmaxusersingroups', get_string('limitmaxusersingroups', 'moodecgrpmanagement'));
        $mform->addHelpButton('limitmaxusersingroups', 'limitmaxusersingroups', 'moodecgrpmanagement');

        $mform->addElement('text', 'maxusersingroups', get_string('generallimitation', 'moodecgrpmanagement'), array('size' => '6'));
        $mform->setType('maxusersingroups', PARAM_INT);
        $mform->disabledIf('maxusersingroups', 'limitmaxusersingroups', 'eq', 0);
        $mform->addRule('maxusersingroups', get_string('error'), 'numeric', 'extraruledata', 'client', false, false);
        $mform->setDefault('maxusersingroups', 0);
        $mform->addElement('button', 'setlimit', get_string('applytoallgroups', 'moodecgrpmanagement'));
        $mform->disabledIf('setlimit', 'limitmaxusersingroups', 'eq', 0);

        // -------------------------
        // Advanced section
        // -------------------------
        $mform->addElement('header', 'advancedsettingshdr', get_string('advancedheader', 'moodecgrpmanagement'));

        $mform->addElement('checkbox', 'displaygrouppicture', get_string('displaygrouppicture', 'moodecgrpmanagement'));
        $mform->setDefault('displaygrouppicture', 'checked');

        $mform->addElement('checkbox', 'displaygroupvideo', get_string('displaygroupvideo', 'moodecgrpmanagement'));
        $mform->setDefault('displaygroupvideo', 'checked');

        $mform->addElement('html', '<fieldset class="clearfix">
				<div class="fcontainer clearfix">
				<div id="fitem_id_option_0" class="fitem fitem_fselect ">
				<div class="fitemtitle"><label for="id_option_0">'.get_string('groupsheader', 'moodecgrpmanagement').'</label><span class="helptooltip"><a href="'. $CFG->wwwroot .'/help.php?component=moodecgrpmanagement&amp;identifier=moodecgrpmanagementoptions&amp;lang=en" title="Help with Choice options" aria-haspopup="true" target="_blank"><img src="'.$CFG->wwwroot.'/theme/image.php?theme='.$PAGE->theme->name.'&component=core&image=help" alt="Help with Choice options" class="iconhelp"></a></span></div><div class="felement fselect">

				<table><tr><td>'.get_string('available_groups', 'moodecgrpmanagement').'</td><td>&nbsp;</td><td>'.get_string('selected_groups', 'moodecgrpmanagement').'</td><td>&nbsp;</td></tr><tr><td style="vertical-align: top">');

        $mform->addElement('html','<select id="availablegroups" name="availableGroups" multiple style="width:200px; heigth:400px">');

        if($groupings) {
            foreach ($groupings as $groupingID => $grouping) {
                // find all linked groups to this grouping
                if (isset($grouping->linkedGroupsIDs) && count($grouping->linkedGroupsIDs) >= 1) { // grouping has more than 2 items, thus we should display it (otherwise it would be clearer to display only that single group alone)

                    $mform->addElement('html', '<option value="' . $groupingID . '" style="font-weight: bold" class="grouping">' . get_string('char_bullet_expanded', 'moodecgrpmanagement') . $grouping->name . '</option>');
                    foreach ($grouping->linkedGroupsIDs as $linkedGroupID) {
                        $mform->addElement('html', '<option value="' . $linkedGroupID . '" class="group nested">&nbsp;&nbsp;&nbsp;&nbsp;' . $groups[$linkedGroupID]->name . '</option>');
                        $groups[$linkedGroupID]->mentioned = true;

                    }
                }
            }
        }

        if(isset($groups)) {
            foreach ($groups as $group) {
                if ($group->mentioned == false) { //Adding Group in System

                    $mform->addElement('html', '<option value="' . $group->id . '" >' . $group->name . '</option>');

                }

            }

        }else {
            $mform->addElement('html', '<option value="defaultemptychoice" class="group nested">&nbsp;&nbsp;&nbsp;&nbsp; empty choice </option>');
        }



          $mform->addElement('html','</select><br><button name="expandButton" type="button" disabled id="expandButton">'.get_string('expand_all_groupings', 'moodecgrpmanagement').'</button><button name="collapseButton" type="button" disabled id="collapseButton">'.get_string('collapse_all_groupings', 'moodecgrpmanagement').'</button><br>'.get_string('double_click_grouping_legend', 'moodecgrpmanagement').'<br>'.get_string('double_click_group_legend', 'moodecgrpmanagement'));

        $mform->addElement('html','
				</td><td><button id="addGroupButton" name="add" type="button" disabled>'.get_string('add', 'moodecgrpmanagement').'</button><div><button name="remove" type="button" disabled id="removeGroupButton">'.get_string('del', 'moodecgrpmanagement').'</button></div></td>');
        $mform->addElement('html','<td style="vertical-align: top"><select id="id_selectedGroups" name="selectedGroups" multiple style="width:200px; heigth:100%">');

        if (!isset($this->_instance) || empty($this->_instance)) {
            if($groups) {
                foreach ($groups as $group) {
                    if ($group->mentioned === false) {
                      //  $mform->addElement('html', '<option value="' . $group->id . '" class="group toplevel">' . $group->name . '</option>');
                    }
                }
            }
        }

        $mform->addElement('html','</select></td><td><div><div id="fitem_id_limit_0" class="fitem fitem_ftext" style="display:none"><div class=""><label for="id_limit_0" id="label_for_limit_ui">'.get_string('set_limit_for_group', 'moodecgrpmanagement').'</label></div><div class="ftext">
				<input class="mod-moodecgrpmanagement-limit-input" type="text" value="0" id="ui_limit_input" disabled="disabled"></div></div></div></td></tr></table>
				</div></div>

				</div>
				</fieldset>');


        if($groups) {
            foreach ($groups as $group) {
                $mform->addElement('hidden', 'group_' . $group->id . '_limit', '', array('id' => 'group_' . $group->id . '_limit', 'class' => 'limit_input_node'));
                $mform->setType('group_' . $group->id . '_limit', PARAM_RAW);
            }
        }

        /*$mform->addElement('selectyesno', 'showunanswered', get_string("showunanswered", "moodecgrpmanagement"));

        $mform->addElement('select', 'showresults', get_string("publish", "moodecgrpmanagement"), $moodecgrpmanagement_SHOWRESULTS);
        $mform->setDefault('showresults', moodecgrpmanagement_SHOWRESULTS_DEFAULT);

        $mform->addElement('select', 'publish', get_string("privacy", "moodecgrpmanagement"), $moodecgrpmanagement_PUBLISH, moodecgrpmanagement_PUBLISH_DEFAULT);
        $mform->setDefault('publish', moodecgrpmanagement_PUBLISH_DEFAULT);
        $mform->disabledIf('publish', 'showresults', 'eq', 0);*/

        $mform->addElement('selectyesno', 'freezegroups', get_string('freezegroups', 'moodecgrpmanagement'));


       // $mform->addElement('date_time_selector', 'freezegroupsaftertime', get_string('freezegroupsaftertime', 'moodecgrpmanagement'), array('optional' => true));

        //-------------------------------------------------------------------------------

        $mform->addElement('hidden', 'serializedselectedgroups', $serializedselectedgroupsValue, array('id' => 'serializedselectedgroups'));
        $mform->setType('serializedselectedgroups', PARAM_RAW);

        $mform->addElement('header', 'timerestricthdr', get_string('timerestrict', 'moodecgrpmanagement'));

        $mform->addElement('checkbox', 'timerestrict', get_string('timerestrict', 'moodecgrpmanagement'));

        $mform->addElement('date_time_selector', 'timeopen', get_string("moodecgrpmanagementopen", "moodecgrpmanagement"));
        $mform->disabledIf('timeopen', 'timerestrict');
/*
        $mform->addElement('date_time_selector', 'timeclose', get_string("moodecgrpmanagementclose", "moodecgrpmanagement"));
        $mform->disabledIf('timeclose', 'timerestrict');
*/
        //-------------------------------------------------------------------------------
        $this->standard_coursemodule_elements();
        //-------------------------------------------------------------------------------
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values) {
        global $DB;
        $this->js_call();

        if (empty($default_values['timeopen'])) {
            $default_values['timerestrict'] = 0;
        } else {
            $default_values['timerestrict'] = 1;
        }
    }

    function validation($data, $files) {
        $errors = parent::validation($data, $files);

       if($data['serializedselectedgroups']) {

            $groupIDs = explode(';', $data['serializedselectedgroups']);
            $groupIDs = array_diff($groupIDs, array(''));
        }/*
        if (array_key_exists('multipleenrollmentspossible', $data) && $data['multipleenrollmentspossible'] === '1') {
            if (count($groupIDs) < 1) {
                $errors['serializedselectedgroups'] = get_string('fillinatleastoneoption', 'moodecgrpmanagement');
            }
        } else {
            if (count($groupIDs) < 2) {
                $errors['serializedselectedgroups'] = get_string('fillinatleasttwooptions', 'moodecgrpmanagement');
            }
        }*/


        return $errors;
    }

    function get_data() {
        $data = parent::get_data();
        if (!$data) {
            return false;
        }
        // Set up completion section even if checkbox is not ticked
        if (empty($data->completionsection)) {
            $data->completionsection=0;
        }
        return $data;
    }

    function add_completion_rules() {
        $mform =& $this->_form;

        $mform->addElement('checkbox', 'completionsubmit', '', get_string('completionsubmit', 'moodecgrpmanagement'));
        return array('completionsubmit');
    }

    function completion_rule_enabled($data) {
        return !empty($data['completionsubmit']);
    }

    public function js_call() {
        global $PAGE;
        $PAGE->requires->yui_module('moodle-mod_moodecgrpmanagement-form', 'Y.Moodle.mod_moodecgrpmanagement.form.init');
        foreach (array_keys(get_string_manager()->load_component_strings('moodecgrpmanagement', current_language())) as $string) {
            $PAGE->requires->string_for_js($string, 'moodecgrpmanagement');
        }
    }

}

