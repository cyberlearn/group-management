<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package    mod
 * @subpackage moodecgrpmanagement
 * @copyright  2013 Université de Lausanne
 * @author     Nicolas Dunand <Nicolas.Dunand@unil.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @modifed by Martin Tazlari
 * @copyright 2016 Cyberlearn
 */

defined('MOODLE_INTERNAL') || die();

/** @global int $moodecgrpmanagement_COLUMN_HEIGHT */
global $moodecgrpmanagement_COLUMN_HEIGHT;
$moodecgrpmanagement_COLUMN_HEIGHT = 300;

/** @global int $moodecgrpmanagement_COLUMN_WIDTH */
global $moodecgrpmanagement_COLUMN_WIDTH;
$moodecgrpmanagement_COLUMN_WIDTH = 300;

define('moodecgrpmanagement_PUBLISH_ANONYMOUS', '0');
define('moodecgrpmanagement_PUBLISH_NAMES',     '1');
define('moodecgrpmanagement_PUBLISH_DEFAULT',   '1');

define('moodecgrpmanagement_SHOWRESULTS_NOT',          '0');
define('moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER', '1');
define('moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE',  '2');
define('moodecgrpmanagement_SHOWRESULTS_ALWAYS',       '3');
define('moodecgrpmanagement_SHOWRESULTS_DEFAULT',      '3');

define('moodecgrpmanagement_DISPLAY_HORIZONTAL',  '0');
define('moodecgrpmanagement_DISPLAY_VERTICAL',    '1');

/** @global array $moodecgrpmanagement_PUBLISH */
global $moodecgrpmanagement_PUBLISH;
$moodecgrpmanagement_PUBLISH = array (moodecgrpmanagement_PUBLISH_ANONYMOUS  => get_string('publishanonymous', 'moodecgrpmanagement'),
                         moodecgrpmanagement_PUBLISH_NAMES      => get_string('publishnames', 'moodecgrpmanagement'));

/** @global array $moodecgrpmanagement_SHOWRESULTS */
global $moodecgrpmanagement_SHOWRESULTS;
$moodecgrpmanagement_SHOWRESULTS = array (moodecgrpmanagement_SHOWRESULTS_NOT          => get_string('publishnot', 'moodecgrpmanagement'),
                         moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER => get_string('publishafteranswer', 'moodecgrpmanagement'),
                         moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE  => get_string('publishafterclose', 'moodecgrpmanagement'),
                         moodecgrpmanagement_SHOWRESULTS_ALWAYS       => get_string('publishalways', 'moodecgrpmanagement'));

/** @global array $moodecgrpmanagement_DISPLAY */
global $moodecgrpmanagement_DISPLAY;
$moodecgrpmanagement_DISPLAY = array (moodecgrpmanagement_DISPLAY_HORIZONTAL   => get_string('displayhorizontal', 'moodecgrpmanagement'),
                         moodecgrpmanagement_DISPLAY_VERTICAL     => get_string('displayvertical','moodecgrpmanagement'));

require_once($CFG->dirroot.'/group/lib.php');

/// Standard functions /////////////////////////////////////////////////////////

/**
 * @global object
 * @param object $course
 * @param object $user
 * @param object $mod
 * @param object $moodecgrpmanagement
 * @return object|null
 */
function moodecgrpmanagement_user_outline($course, $user, $mod, $moodecgrpmanagement) {
    if ($groupmembership = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $user)) { // if user has answered
        $result = new stdClass();
        $result->info = "'".format_string($groupmembership->name)."'";
        $result->time = $groupmembership->timeuseradded;
        return $result;
    }
    return NULL;
}

/**
 *
 */
function moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $user, $returnArray = FALSE, $refresh = FALSE) {
    global $DB, $moodecgrpmanagement_groups;


    static $user_answers = array();

    if (is_numeric($user)) {
        $userid = $user;
    }
    else {
        $userid = $user->id;
    }

    if (!$refresh and isset($user_answers[$userid])) {
        if ($returnArray === TRUE) {
            return $user_answers[$userid];
        } else {
            return $user_answers[$userid][0];
        }
    } else {
        $user_answers = array();
    }

    if (!count($moodecgrpmanagement_groups)) {
        $moodecgrpmanagement_groups = moodecgrpmanagement_get_groups($moodecgrpmanagement);
    }

    $groupids = array();
    foreach ($moodecgrpmanagement_groups as $group) {
        if (is_numeric($group->id)) {
            $groupids[] = $group->id;
        }
    }
    if ($groupids) {
        $params1 = array($userid);
        list($insql, $params2) = $DB->get_in_or_equal($groupids);
        $params = array_merge($params1, $params2);
        $groupmemberships = $DB->get_records_sql('SELECT * FROM {groups_members} WHERE userid = ? AND groupid '.$insql, $params);
        $groups = array();
        foreach ($groupmemberships as $groupmembership) {
            $group = $moodecgrpmanagement_groups[$groupmembership->groupid];
            $group->timeuseradded = $groupmembership->timeadded;
            $groups[] = $group;
        }
        if (count($groups) > 0) {
            $user_answers[$userid] = $groups;
            if ($returnArray === TRUE) {
                return $groups;
            } else {
                return $groups[0];
            }
        }
    }
    return false;

}

/**
 * @global object
 * @param object $course
 * @param object $user
 * @param object $mod
 * @param object $moodecgrpmanagement
 * @return string|void
 */
function moodecgrpmanagement_user_complete($course, $user, $mod, $moodecgrpmanagement) {
    if ($groupmembership = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $user)) { // if user has answered
        $result = new stdClass();
        $result->info = "'".format_string($groupmembership->name)."'";
        $result->time = $groupmembership->timeuseradded;
        echo get_string("answered", "moodecgrpmanagement").": $result->info. ".get_string("updated", '', userdate($result->time));
    } else {
        print_string("notanswered", "moodecgrpmanagement");
    }
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @global object
 * @param object $moodecgrpmanagement
 * @return int
 */
function moodecgrpmanagement_add_instance($moodecgrpmanagement) {
    global $DB, $USER;

    $moodecgrpmanagement->timemodified = time();
  	
  	$moodecgrpmanagement->publish = 1 ;
    $moodecgrpmanagement->showresults = 3 ;
    $moodecgrpmanagement->showunanswered = 1 ;
  

    if (empty($moodecgrpmanagement->timerestrict)) {
        $moodecgrpmanagement->timeopen = 0;
        $moodecgrpmanagement->timeclose = 0;
    }


    //insert answers
    $moodecgrpmanagement->id = $DB->insert_record("moodecgrpmanagement", $moodecgrpmanagement);
    
    // deserialize the selected groups


        $groupIDs = explode(';', $moodecgrpmanagement->serializedselectedgroups);
        $groupIDs = array_diff($groupIDs, array(''));


        foreach ($groupIDs as $groupID) {
            $groupID = trim($groupID);



            if (isset($groupID) && $groupID != '') {
                $option = new stdClass();
                $option->groupid = $groupID;
                $option->moodecgrpmanagementid = $moodecgrpmanagement->id;
                $option->creatorid = $USER->id;
                $option->enrollementkey = null ;
                // Select si existe la clé d'inscription

                $param = array($groupID);
                $query = "SELECT enrolmentkey FROM {groups} WHERE id= ?";
                $itemid = $DB->get_fieldset_sql($query, $param);

                // ajouter l'option dans les options

                if(isset($itemid[0])){
                    $option->enrollementkey = $itemid[0] ;
                }

                $property = 'group_' . $groupID . '_limit';
                if (isset($moodecgrpmanagement->$property)) {
                    $option->maxanswers = $moodecgrpmanagement->$property;
                }
                $option->timemodified = time();
                $DB->insert_record("moodecgrpmanagement_options", $option);
            }

    }
    
    return $moodecgrpmanagement->id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @global object
 * @param object $moodecgrpmanagement
 * @return bool
 */
function moodecgrpmanagement_update_instance($moodecgrpmanagement) {
    global $DB, $USER;

    $moodecgrpmanagement->id = $moodecgrpmanagement->instance;
    $moodecgrpmanagement->timemodified = time();


    if (empty($moodecgrpmanagement->timerestrict)) {
        $moodecgrpmanagement->timeopen = 0;
        $moodecgrpmanagement->timeclose = 0;
    }

    if (empty($moodecgrpmanagement->multipleenrollmentspossible)) {
        $moodecgrpmanagement->multipleenrollmentspossible = 0;
    }

    if (empty($moodecgrpmanagement->limitmaxusersingroups)) {
        $moodecgrpmanagement->limitmaxusersingroups = 0;
    }

    if (empty($moodecgrpmanagement->maxusersingroups)) {
        $moodecgrpmanagement->maxusersingroups = 0;
    }    

    if (empty($moodecgrpmanagement->freezegroups)) {
        $moodecgrpmanagement->freezegroups = 0;
    }

    if (empty($moodecgrpmanagement->freezegroupsaftertime)) {
        $moodecgrpmanagement->freezegroupsaftertime = null;
    }

    if (empty($moodecgrpmanagement->displaygrouppicture)) {
        $moodecgrpmanagement->displaygrouppicture = 0;
    }

    if (empty($moodecgrpmanagement->displaygroupvideo)) {
        $moodecgrpmanagement->displaygroupvideo = 0;
    }

    if (empty($moodecgrpmanagement->groupcreationpossible)) {
        $moodecgrpmanagement->groupcreationpossible = 0;
    }

    if (empty($moodecgrpmanagement->privategroupspossible)) {
        $moodecgrpmanagement->privategroupspossible = 0;
    }

    if (empty($moodecgrpmanagement->limitmaxgroups)) {
        $moodecgrpmanagement->limitmaxgroups = 0;
    }

    if (empty($moodecgrpmanagement->maxgroups)) {
        $moodecgrpmanagement->maxgroups = 0;
    }

    // deserialize the selected groups
    
    $groupIDs = explode(';', $moodecgrpmanagement->serializedselectedgroups);
    $groupIDs = array_diff( $groupIDs, array( '' ) );

    // prepare pre-existing selected groups from database
    
   $preExistingGroups = $DB->get_records("moodecgrpmanagement_options", array("moodecgrpmanagementid" => $moodecgrpmanagement->id), "id");



if($groupIDs) {
    // walk through form-selected groups
    foreach ($groupIDs as $groupID) {
        $groupID = trim($groupID);
        if (isset($groupID) && $groupID != '') {
            $option = new stdClass();
            $option->groupid = $groupID;
            $option->moodecgrpmanagementid = $moodecgrpmanagement->id;
            $property = 'group_' . $groupID . '_limit';
            $option->creatorid = $USER->id ;
            if (isset($moodecgrpmanagement->$property)) {
                $option->maxanswers = $moodecgrpmanagement->$property;
            }

            $option->timemodified = time();

            $creatorids = $DB->get_records("moodecgrpmanagement_options", array("groupid" => $groupID), "creatorid");

            foreach($creatorids as $creatorid){
                if($creatorid->creatorid){
                    $option->creatorid = $creatorid->creatorid ;
                    break ;
                }
                else {
                    $option->creatorid = $USER->id ;
                }
            }

            // Find out if this selection already exists

            foreach ($preExistingGroups as $key => $preExistingGroup) {


                if ($option->groupid == $preExistingGroup->groupid) {
                    // match found, so instead of creating a new record we should merely update a pre-existing record
                    $option->maxanswers = $moodecgrpmanagement->maxusersingroups;
                    $option->creatorid = $preExistingGroup->creatorid ;
                    $option->id = $preExistingGroup->id;
                    $DB->update_record("moodecgrpmanagement_options", $option);
                    // remove the element from the array to not deal with it later
                    unset($preExistingGroups[$key]);
                    continue 2; // continue the big loop
                }
            }
            $DB->insert_record("moodecgrpmanagement_options", $option);
        }

    }
}
    // remove all remaining pre-existing groups which did not appear in the form (and are thus assumed to have been deleted)
    foreach ($preExistingGroups as $preExistingGroup) {
    	$DB->delete_records("moodecgrpmanagement_options", array("id"=>$preExistingGroup->id));
    }
	
  	$moodecgrpmanagement->publish = 1 ;
    $moodecgrpmanagement->showresults = 3 ;
    $moodecgrpmanagement->showunanswered = 1 ;
  
    return $DB->update_record('moodecgrpmanagement', $moodecgrpmanagement);

}

/**
 * @global object
 * @param object $moodecgrpmanagement
 * @param object $user
 * @param object $coursemodule
 * @param array $allresponses
 * @return array
 */
function moodecgrpmanagement_prepare_options($moodecgrpmanagement, $user, $coursemodule, $allresponses) {

    $cdisplay = array('options'=>array());

    $cdisplay['limitmaxusersingroups'] = true;
    $context = context_module::instance($coursemodule->id);
    $answers = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $user, TRUE);

    if(isset($moodecgrpmanagement->option)) {
        foreach ($moodecgrpmanagement->option as $optionid => $text) {
            if (isset($text)) { //make sure there are no dud entries in the db with blank text values.
                $option = new stdClass;
                $option->attributes = new stdClass;
                $option->attributes->value = $optionid;
                $option->groupid = $text;
                $option->maxanswers = $moodecgrpmanagement->maxanswers[$optionid];
                $option->groupvideo = $moodecgrpmanagement->groupvideo[$optionid];
                $option->creatorid = $moodecgrpmanagement->creatorid[$optionid];
                $option->enrollementkey = $moodecgrpmanagement->enrollementkey[$optionid];
                $option->displaylayout = $moodecgrpmanagement->display;

                if (isset($allresponses[$text])) {
                    $option->countanswers = count($allresponses[$text]);
                } else {
                    $option->countanswers = 0;
                }
                if (is_array($answers)) {
                    foreach ($answers as $answer) {
                        if ($answer && $text == $answer->id) {
                            $option->attributes->checked = true;
                        }
                    }
                }
                if ($moodecgrpmanagement->limitmaxusersingroups && ($option->countanswers >= $option->maxanswers) && empty($option->attributes->checked)) {
                    $option->attributes->disabled = true;
                }
                $cdisplay['options'][] = $option;
            }
        }
    }

    $cdisplay['hascapability'] = is_enrolled($context, NULL, 'mod/moodecgrpmanagement:choose'); //only enrolled users are allowed to make a moodecgrpmanagement

    if ($moodecgrpmanagement->allowupdate && is_array($answers)) {
        $cdisplay['allowupdate'] = true;
    }

    return $cdisplay;
}

/**
 * @param $url
 * @return mixed|string
 * Method that clean the iframe from the user to keep only needed part
 */
function iframe_clean($url) {

    $url = strip_tags($url,"<iframe>");  // interpret and keep only iframe tags

    $url = strstr($url, '<iframe'); // Delete everything before iframe
    $length = strpos($url, "iframe>")+7 ;
    $url = substr($url, 0, $length); // Delete everything after iframe
    if($url == "0"){ // Check if no balise iframe
        $url = "" ;
    }
    return $url ;

}

/**
 * @global object
 * @param int $formanswer
 * @param object $moodecgrpmanagement
 * @param int $userid
 * @param object $course Course object
 * @param object $cm
 */
function moodecgrpmanagement_user_submit_response($formanswer, $moodecgrpmanagement, $userid, $course, $cm) {
    global $DB, $CFG;
    require_once($CFG->libdir.'/completionlib.php');

    $context = context_module::instance($cm->id);
    $eventparams = array(
        'context' => $context,
        'objectid' => $moodecgrpmanagement->id
    );

    $selected_option = $DB->get_record('moodecgrpmanagement_options', array('id' => $formanswer));

    $current = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $userid);
    if ($current) {
        $currentgroup = $DB->get_record('groups', array('id' => $current->id), 'id,name', MUST_EXIST);
    }
    $selectedgroup = $DB->get_record('groups', array('id' => $selected_option->groupid), 'id,name', MUST_EXIST);

    $countanswers=0;
    if($moodecgrpmanagement->limitmaxusersingroups) {
        $groupmembers = $DB->get_records('groups_members', array('groupid' => $selected_option->groupid));
        $countanswers = count($groupmembers);
        $maxans = $moodecgrpmanagement->maxanswers[$formanswer];
    }

    if (!($moodecgrpmanagement->limitmaxusersingroups && ($countanswers >= $maxans) )) {
        groups_add_member($selected_option->groupid, $userid);
        if ($current) {
            if (!($moodecgrpmanagement->multipleenrollmentspossible == 1)) {
                if ($selected_option->groupid != $current->id) {
                    if (groups_is_member($current->id, $userid)) {
                        groups_remove_member($current->id, $userid);
//                        $eventparams['groupname'] = $currentgroup->name;
                        $event = \mod_moodecgrpmanagement\event\choice_removed::create($eventparams);
                        $event->add_record_snapshot('course_modules', $cm);
                        $event->add_record_snapshot('course', $course);
                        $event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
                        $event->trigger();
                    }
                }
            }
        } else {
            // Update completion state
            $completion = new completion_info($course);
            if ($completion->is_enabled($cm) && $moodecgrpmanagement->completionsubmit) {
                $completion->update_state($cm, COMPLETION_COMPLETE);
            }
//            $eventparams['groupname'] = $selectedgroup->name;
            $event = \mod_moodecgrpmanagement\event\choice_updated::create($eventparams);
            $event->add_record_snapshot('course_modules', $cm);
            $event->add_record_snapshot('course', $course);
            $event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
            $event->trigger();
        }
    } else {
        if (!$current || !($current->id==$selected_option->groupid)) { //check to see if current moodecgrpmanagement already selected - if not display error
           // print_error('moodecgrpmanagementfull', 'moodecgrpmanagement', $CFG->wwwroot.'/mod/moodecgrpmanagement/view.php?id='.$cm->id);
        }
    }
}

/**
 * @param object $moodecgrpmanagement
 * @param array $allresponses
 * @param object $cm
 * @return void Output is echo'd
 */
function moodecgrpmanagement_show_reportlink($moodecgrpmanagement, $allresponses, $cm) {
    $responsecount = 0;
    $respondents = array();
    foreach($allresponses as $optionid => $userlist) {
        if ($optionid) {
            $responsecount += count($userlist);
            if ($moodecgrpmanagement->multipleenrollmentspossible) {
                foreach ($userlist as $user) {
                    if (!in_array($user->id, $respondents)) {
                        $respondents[] = $user->id;
                    }
                }
            }
        }
    }
    echo '<div class="reportlink"><a href="report.php?id='.$cm->id.'">'.get_string("viewallresponses", "moodecgrpmanagement", $responsecount);
    if ($moodecgrpmanagement->multipleenrollmentspossible == 1) {
        echo ' ' . get_string("byparticipants", "moodecgrpmanagement", count($respondents));
    }
    echo '</a></div>';
}

/**
 * @global object
 * @param object $moodecgrpmanagement
 * @param object $course
 * @param object $coursemodule
 * @param array $allresponses

 *  * @param bool $allresponses
 * @return object
 */
function prepare_moodecgrpmanagement_show_results($moodecgrpmanagement, $course, $cm, $allresponses, $forcepublish=false) {
    global $CFG, $FULLSCRIPT, $PAGE, $OUTPUT, $DB;

    $display = clone($moodecgrpmanagement);
    $display->coursemoduleid = $cm->id;
    $display->courseid = $course->id;
//debugging('<pre>'.print_r($moodecgrpmanagement->option, true).'</pre>', DEBUG_DEVELOPER);
   // debugging('<pre>'.print_r($allresponses, true).'</pre>', DEBUG_DEVELOPER);

    //overwrite options value;
    $display->options = array();
    $totaluser = 0;
    foreach ($moodecgrpmanagement->option as $optionid => $groupid) {
        $display->options[$optionid] = new stdClass;
        $display->options[$optionid]->groupid = $groupid;
        $display->options[$optionid]->maxanswer = $moodecgrpmanagement->maxanswers[$optionid];

        if (array_key_exists($groupid, $allresponses)) {
            $display->options[$optionid]->user = $allresponses[$groupid];
            $totaluser += count($allresponses[$groupid]);
        }
    }
    if ($moodecgrpmanagement->showunanswered) {
        $display->options[0]->user = $allresponses[0];
    }
    unset($display->option);
    unset($display->maxanswers);

    $display->numberofuser = $totaluser;
    $context = context_module::instance($cm->id);
    $display->viewresponsecapability = has_capability('mod/moodecgrpmanagement:readresponses', $context);
    $display->deleterepsonsecapability = has_capability('mod/moodecgrpmanagement:deleteresponses',$context);
    $display->fullnamecapability = has_capability('moodle/site:viewfullnames', $context);

    if (empty($allresponses)) {
        echo $OUTPUT->heading(get_string("nousersyet"));
        return false;
    }


    $totalresponsecount = 0;
    foreach ($allresponses as $optionid => $userlist) {
        if ($moodecgrpmanagement->showunanswered || $optionid) {
            $totalresponsecount += count($userlist);
        }
    }

    $context = context_module::instance($cm->id);

    $hascapfullnames = has_capability('moodle/site:viewfullnames', $context);

    $viewresponses = has_capability('mod/moodecgrpmanagement:readresponses', $context);

    foreach($allresponses as $key=>$response) {

        if($key == 0){
            if(count($response)>0) {
                echo "<h1>" . get_string('notingroup','moodecgrpmanagement') ."</h1>";
                echo html_writer::table(display_ungroup_member_report($response, $course->id));
            }

        } else {
            $group = $DB->get_record("groups",array("id"=>$key)) ;
            echo "<h1>". $group->name . "</h1>";
            echo html_writer::table(display_group_member_report($key, $course->id));
        }



    }


/*
    switch ($forcepublish) {
        case moodecgrpmanagement_PUBLISH_NAMES:
            echo '<div id="tablecontainer">';
            if ($viewresponses) {
                echo '<form id="attemptsform" method="post" action="'.$FULLSCRIPT.'" onsubmit="var menu = document.getElementById(\'menuaction\'); return (menu.options[menu.selectedIndex].value == \'delete\' ? \''.addslashes_js(get_string('deleteattemptcheck','quiz')).'\' : true);">';
                echo '<div>';
                echo '<input type="hidden" name="id" value="'.$cm->id.'" />';
                echo '<input type="hidden" name="sesskey" value="'.sesskey().'" />';
                echo '<input type="hidden" name="mode" value="overview" />';
            }







            echo "<table cellpadding=\"5\" cellspacing=\"10\" class=\"results names\">";



            echo "<tr>";

            $columncount = array(); // number of votes in each column
            if ($moodecgrpmanagement->showunanswered) {
                $columncount[0] = 0;
                echo "<th class=\"col0 header\" scope=\"col\">";
                print_string('notanswered', 'moodecgrpmanagement');
                echo "</th>";
            }
            $count = 1;
            foreach ($moodecgrpmanagement->option as $optionid => $optiontext) {
                $columncount[$optionid] = 0; // init counters
                echo "<th class=\"col$count header\" scope=\"col\">";
                echo format_string($optiontext);
                echo "</th>";
                $count++;
            }
            echo "</tr><tr>";

            if ($moodecgrpmanagement->showunanswered) {
                echo "<td class=\"col$count data\" >";
                // added empty row so that when the next iteration is empty,
                // we do not get <table></table> error from w3c validator
                // MDL-7861
                echo "<table class=\"moodecgrpmanagementresponse\"><tr><td></td></tr>";
                if (!empty($allresponses[0])) {
                    foreach ($allresponses[0] as $user) {
                        echo "<tr>";
                        echo "<td class=\"picture\">";
                        echo $OUTPUT->user_picture($user, array('courseid'=>$course->id));
                        echo "</td><td class=\"fullname\">";
                        echo "<a href=\"$CFG->wwwroot/user/view.php?id=$user->id&amp;course=$course->id\">";
                        echo fullname($user, $hascapfullnames);
                        echo "</a>";
                        echo "</td></tr>";
                    }
                }
                echo "</table></td>";
            }
            $count = 1;
            foreach ($moodecgrpmanagement->option as $optionid => $optiontext) {
                    echo '<td class="col'.$count.' data" >';

                    // added empty row so that when the next iteration is empty,
                    // we do not get <table></table> error from w3c validator
                    // MDL-7861
                    echo '<table class="moodecgrpmanagementresponse"><tr><td></td></tr>';

                    if (isset($allresponses[$optionid])) {
                        foreach ($allresponses[$optionid] as $user) {
                            $columncount[$optionid] += 1;
                            echo '<tr><tr class="attemptcell">';
                            if ($viewresponses and has_capability('mod/moodecgrpmanagement:deleteresponses',$context)) {
                                echo '<input type="checkbox" name="userid[]" value="'. $user->id. '" />';
                            }
                            echo '</tr><tr class="picture">';
                            echo $OUTPUT->user_picture($user, array('courseid'=>$course->id));
                            echo '</tr><tr class="fullname">';
                            echo "<a href=\"$CFG->wwwroot/user/view.php?id=$user->id&amp;course=$course->id\">";
                            echo fullname($user, $hascapfullnames);
                            echo '</a>';
                            echo '</tr></tr>';
                       }
                    }
                    $count++;
                    echo '</table></td>';
            }
            echo "</tr><tr>";
            $count = 1;

            if ($moodecgrpmanagement->showunanswered) {
                echo "<td></td>";
            }

            foreach ($moodecgrpmanagement->option as $optionid => $optiontext) {
                echo "<td align=\"center\" class=\"col$count count\">";
                if ($moodecgrpmanagement->limitmaxusersingroups) {
                    echo get_string("taken", "moodecgrpmanagement").":";
                    echo $columncount[$optionid];
                    echo "<br/>";
                    echo get_string("limit", "moodecgrpmanagement").":";
                    echo $moodecgrpmanagement->maxanswers[$optionid];
                } else {
                    if (isset($columncount[$optionid])) {
                        echo $columncount[$optionid];
                    }
                }
                echo "</td>";
                $count++;
            }
            echo "</tr>";

            /// Print "Select all" etc.
            if ($viewresponses and has_capability('mod/moodecgrpmanagement:deleteresponses',$context)) {
                echo '<tr><td></td><td>';
                echo '<a href="javascript:select_all_in(\'DIV\',null,\'tablecontainer\');">'.get_string('selectall').'</a> / ';
                echo '<a href="javascript:deselect_all_in(\'DIV\',null,\'tablecontainer\');">'.get_string('deselectall').'</a> ';
                echo '&nbsp;&nbsp;';
                echo html_writer::label(get_string('withselected', 'moodecgrpmanagement'), 'menuaction');
                echo html_writer::select(array('delete' => get_string('delete')), 'action', '', array(''=>get_string('withselectedusers')), array('id'=>'menuaction'));
                $PAGE->requires->js_init_call('M.util.init_select_autosubmit', array('attemptsform', 'menuaction', ''));
                echo '<noscript id="noscriptmenuaction" style="display:inline">';
                echo '<div>';
                echo '<input type="submit" value="'.get_string('go').'" /></div></noscript>';
                echo '</td><td></td></tr>';
            }

            echo "</table></div>";
            if ($viewresponses) {
                echo "</form></div>";
            }
            break;
    }
    return $display;*/
}

/**
 * @global object
 * @param array $userids
 * @param object $moodecgrpmanagement Choice main table row
 * @param object $cm Course-module object
 * @param object $course Course object
 * @return bool
 */
function moodecgrpmanagement_delete_responses($userids, $moodecgrpmanagement, $cm, $course) {
    global $CFG, $DB, $context;
    require_once($CFG->libdir.'/completionlib.php');

    if(!is_array($userids) || empty($userids)) {
        return false;
    }

    foreach($userids as $num => $userid) {
        if(empty($userid)) {
            unset($userids[$num]);
        }
    }

    $completion = new completion_info($course);
    $eventparams = array(
        'context' => $context,
        'objectid' => $moodecgrpmanagement->id
    );

    foreach($userids as $userid) {
        if ($current = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $userid)) {
            $currentgroup = $DB->get_record('groups', array('id' => $current->id), 'id,name', MUST_EXIST);
            if (groups_is_member($current->id, $userid)) {
                groups_remove_member($current->id, $userid);
                $event = \mod_moodecgrpmanagement\event\choice_removed::create($eventparams);
                $event->add_record_snapshot('course_modules', $cm);
                $event->add_record_snapshot('course', $course);
                $event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
                $event->trigger();
            }
            // Update completion state
            if ($completion->is_enabled($cm) && $moodecgrpmanagement->completionsubmit) {
                $completion->update_state($cm, COMPLETION_INCOMPLETE, $userid);
            }
        }
    }
    return true;
}


/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @global object
 * @param int $id
 * @return bool
 */
function moodecgrpmanagement_delete_instance($id) {
    global $DB;

    if (! $moodecgrpmanagement = $DB->get_record("moodecgrpmanagement", array("id"=>"$id"))) {
        return false;
    }

    $result = true;

    if (! $DB->delete_records("moodecgrpmanagement_options", array("moodecgrpmanagementid"=>"$moodecgrpmanagement->id"))) {
        $result = false;
    }

    if (! $DB->delete_records("moodecgrpmanagement", array("id"=>"$moodecgrpmanagement->id"))) {
        $result = false;
    }

    return $result;
}

/**
 * Returns text string which is the answer that matches the id
 *
 * @global object
 * @param object $moodecgrpmanagement
 * @param int $id
 * @return string
 */
function moodecgrpmanagement_get_option_text($moodecgrpmanagement, $id) {
    global $DB;

    if ($result = $DB->get_record('groups', array('id' => $id))) {
        return $result->name;
    } else {
        return get_string("notanswered", "moodecgrpmanagement");
    }
}

/*
 * Returns DB records of groups used by the moodecgrpmanagement activity
 *
 * @global object
 * @param object $moodecgrpmanagement
 * @return array
 */
function moodecgrpmanagement_get_groups($moodecgrpmanagement) {
    global $DB;

    static $groups = array();

    if (count($groups)) {
        return $groups;
    }

    if (is_numeric($moodecgrpmanagement)) {
        $moodecgrpmanagementid = $moodecgrpmanagement;
    }
    else {
        $moodecgrpmanagementid = $moodecgrpmanagement->id;
    }

    $groups = array();
    $options = $DB->get_records('moodecgrpmanagement_options', array('moodecgrpmanagementid' => $moodecgrpmanagementid));
    foreach ($options as $option) {
        if ($group = $DB->get_record('groups', array('id' => $option->groupid)))
        $groups[$group->id] = $group;
    }
    return $groups;
}

/**
 * Gets a full moodecgrpmanagement record
 *
 * @global object
 * @param int $moodecgrpmanagementid
 * @return object|bool The moodecgrpmanagement or false
 */
function moodecgrpmanagement_get_moodecgrpmanagement($moodecgrpmanagementid) {
    global $DB;

    if ($moodecgrpmanagement = $DB->get_record("moodecgrpmanagement", array("id" => $moodecgrpmanagementid))) {
        if ($options = $DB->get_records("moodecgrpmanagement_options", array("moodecgrpmanagementid" => $moodecgrpmanagementid), "id")) {
            foreach ($options as $option) {
                $option->enrollementkey = null ;
                $moodecgrpmanagement->option[$option->id] = $option->groupid;
                $moodecgrpmanagement->maxanswers[$option->id] = $option->maxanswers;
                $moodecgrpmanagement->groupvideo[$option->id] = $option->groupvideo;
                $moodecgrpmanagement->creatorid[$option->id] = $option->creatorid;
                $moodecgrpmanagement->enrollementkey[$option->id] = $option->enrollementkey;
            }
            return $moodecgrpmanagement;
        }else {
            return $moodecgrpmanagement;
        }
    }
    return false;
}

/**
 * @return array
 */
function moodecgrpmanagement_get_view_actions() {
    return array('view','view all','report');
}

/**
 * @return array
 */
function moodecgrpmanagement_get_post_actions() {
    return array('choose','choose again');
}


/**
* Implementation of the function for printing the form elements that control
* whether the course reset functionality affects the moodecgrpmanagement.
 *
 * @param object $mform form passed by reference
*/
function moodecgrpmanagement_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'moodecgrpmanagementheader', get_string('modulenameplural', 'moodecgrpmanagement'));
    $mform->addElement('advcheckbox', 'reset_moodecgrpmanagement', get_string('removeresponses','moodecgrpmanagement'));
}
function moodecgrpmanagement_reset_userdata($data) {
    global $DB ;
    $componentstr = get_string('modulenameplural', 'moodecgrpmanagement');
    groups_delete_group_members($data->courseid);
    $status[] = array('component'=>$componentstr, 'item'=>get_string('resetgrpmanagement', 'moodecgrpmanagement'), 'error'=>false);

    return $status;

}


/**
 * Course reset form defaults.
 *
 * @return array
 */
function moodecgrpmanagement_reset_course_form_defaults($course) {
    return array('reset_moodecgrpmanagement'=>1);
}


/**
 * @global object
 * @global object
 * @global object
 * @uses CONTEXT_MODULE
 * @param object $moodecgrpmanagement
 * @param object $cm
 * @return array
 */
function moodecgrpmanagement_get_response_data($moodecgrpmanagement, $cm) {
    global $CFG, $context, $choicegrop_users;

    /// Initialise the returned array, which is a matrix:  $allresponses[responseid][userid] = responseobject
    static $allresponses = array();

    if (count($allresponses)) {
        return $allresponses;
    }

    /// First get all the users who have access here
    /// To start with we assume they are all "unanswered" then move them later
    $context = context_module::instance($cm->id);


    $choicegrop_users = get_enrolled_users($context, 'mod/moodecgrpmanagement:choose', 0, user_picture::fields('u', array('idnumber')), 'u.lastname ASC,u.firstname ASC');
    $allresponses[0] = $choicegrop_users;

    if ($allresponses[0]) {
        // if groupmembersonly used, remove users who are not in any group
        if (!empty($CFG->enablegroupmembersonly) and $cm->groupmembersonly) {
            if ($groupingusers = groups_get_grouping_members($cm->groupingid, 'u.id', 'u.id')) {
                $allresponses[0] = array_intersect_key($allresponses[0], $groupingusers);
            }
        }
    }

    foreach ($allresponses[0] as $user) {
        $currentAnswers = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $user, TRUE);
        if ($currentAnswers != false) {
            foreach ($currentAnswers as $current) {
                $allresponses[$current->id][$user->id] = clone($allresponses[0][$user->id]);
                $allresponses[$current->id][$user->id]->timemodified = $current->timeuseradded;
            }
            unset($allresponses[0][$user->id]);   // Remove from unanswered column
        }
    }
    return $allresponses;
}

/**
 * Returns all other caps used in module
 *
 * @return array
 */
function moodecgrpmanagement_get_extra_capabilities() {
    return array('moodle/site:accessallgroups');
}

/**
 * @uses FEATURE_GROUPS
 * @uses FEATURE_GROUPINGS
 * @uses FEATURE_GROUPMEMBERSONLY
 * @uses FEATURE_MOD_INTRO
 * @uses FEATURE_COMPLETION_TRACKS_VIEWS
 * @uses FEATURE_GRADE_HAS_GRADE
 * @uses FEATURE_GRADE_OUTCOMES
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, null if doesn't know
 */
function moodecgrpmanagement_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:                  return true;
        case FEATURE_GROUPINGS:               return true;
        case FEATURE_GROUPMEMBERSONLY:        return true;
        case FEATURE_MOD_INTRO:               return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: return true;
        case FEATURE_COMPLETION_HAS_RULES:    return true;
        case FEATURE_GRADE_HAS_GRADE:         return false;
        case FEATURE_GRADE_OUTCOMES:          return false;
        case FEATURE_BACKUP_MOODLE2:          return true;
        case FEATURE_SHOW_DESCRIPTION:        return true;

        default: return null;
    }
}

/**
 * Adds module specific settings to the settings block
 *
 * @param settings_navigation $settings The settings navigation object
 * @param navigation_node $moodecgrpmanagementnode The node to add module settings to
 */
function moodecgrpmanagement_extend_settings_navigation(settings_navigation $settings, navigation_node $moodecgrpmanagementnode) {
    global $PAGE;

    if (has_capability('mod/moodecgrpmanagement:readresponses', $PAGE->cm->context)) {

        $groupmode = groups_get_activity_groupmode($PAGE->cm);
        if ($groupmode) {
            groups_get_activity_group($PAGE->cm, true);
        }
        if (!$moodecgrpmanagement = moodecgrpmanagement_get_moodecgrpmanagement($PAGE->cm->instance)) {
            print_error('invalidcoursemodule');
            return false;
        }
        $allresponses = moodecgrpmanagement_get_response_data($moodecgrpmanagement, $PAGE->cm, $groupmode);   // Big function, approx 6 SQL calls per user

        $responsecount = 0;
        $respondents = array();
        foreach($allresponses as $optionid => $userlist) {
            if ($optionid) {
                $responsecount += count($userlist);
                if ($moodecgrpmanagement->multipleenrollmentspossible) {
                    foreach ($userlist as $user) {
                        if (!in_array($user->id, $respondents)) {
                            $respondents[] = $user->id;
                        }
                    }
                }
            }
        }
        $viewallresponsestext = get_string("viewallresponses", "moodecgrpmanagement", $responsecount);
        if ($moodecgrpmanagement->multipleenrollmentspossible == 1) {
            $viewallresponsestext .= ' ' . get_string("byparticipants", "moodecgrpmanagement", count($respondents));
        }
        $moodecgrpmanagementnode->add($viewallresponsestext, new moodle_url('/mod/moodecgrpmanagement/report.php', array('id'=>$PAGE->cm->id)));
    }
}

/**
 * Obtains the automatic completion state for this moodecgrpmanagement based on any conditions
 * in forum settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not, $type if conditions not set.
 */
function moodecgrpmanagement_get_completion_state($course, $cm, $userid, $type) {
    global $DB;

    // Get moodecgrpmanagement details
    $moodecgrpmanagement = $DB->get_record('moodecgrpmanagement', array('id'=>$cm->instance), '*', MUST_EXIST);

    // If completion option is enabled, evaluate it and return true/false
    if($moodecgrpmanagement->completionsubmit) {
        $useranswer = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $userid);
        return $useranswer !== false;
    } else {
        // Completion option is not enabled so just return $type
        return $type;
    }
}


/**
 * Return a list of page types
 * @param string $pagetype current page type
 * @param stdClass $parentcontext Block's parent context
 * @param stdClass $currentcontext Current context of block
 */
function moodecgrpmanagement_page_type_list($pagetype, $parentcontext, $currentcontext) {
    $module_pagetype = array('mod-moodecgrpmanagement-*'=>get_string('page-mod-moodecgrpmanagement-x', 'choice'));
    return $module_pagetype;
}

function display_group_ip_location($groupid){
    global $DB, $USER, $CFG, $PAGE ;

    include("geoloc/geoipcity.inc");
    include("geoloc/geoipregionvars.php");
    $gi = geoip_open($CFG->dirroot.'/mod/moodecgrpmanagement/geoloc/GeoLiteCity.dat',GEOIP_STANDARD);

    $members = groups_get_members_ip($groupid, $fields='u.lastip');
    $records = array();
    foreach($members as $member){
        $record = geoip_record_by_addr($gi,$member->lastip);
        array_push($records,$record);
    }
    geoip_close($gi);
    return $records ;
}

function groups_get_members_ip($groupid, $fields='u.*', $sort='lastname ASC') {
    global $DB;

    return $DB->get_records_sql("SELECT DISTINCT $fields
                                   FROM {user} u, {groups_members} gm
                                  WHERE u.id = gm.userid AND gm.groupid = ?
                               ORDER BY $sort", array($groupid));
}

function display_group_map($groupid) {

    global $DB, $USER, $CFG, $PAGE ;
	
	$apikey = get_config('moodecgrpmanagement', 'googleapikey'); ;
	$mapurl = "https://maps.googleapis.com/maps/api/js?key=".$apikey."&callback=initMap";

    $map =  '
    <div id="map"></div>
    <script type="text/javascript">

        function initMap() {

        var groupid = $("#groupid").val();
		
        $.ajax({
        url : "./geolocservice.php?id="+groupid,
        type : "POST",
        dataType : "json",
        data : {
        },
        success : function(json) {

         // Create an array of styles.
                  var styles = [
                    {
                      stylers: [
												{ hue: "#00ffd2" },
                        { saturation: 0 },
												{ lightness: 0 }
                      ]
                    },{
                      featureType: "road",
                      elementType: "geometry",
                      stylers: [
                        { lightness: 300 },
                        { visibility: "off" }
                      ]
                    },{
                      featureType: "road",
                      elementType: "labels",
                      stylers: [
                        { visibility: "off" }
                      ]
                    }
                  ];

                  // Create a new StyledMapType object, passing it the array of styles,
                  // as well as the name to be displayed on the map type control.
                  var styledMap = new google.maps.StyledMapType(styles,
                    {name: "Styled Map"});

            if(json.length!=0){

                var markerimage = "../pix/position.png" ;
             
                var myLatLng = {lat: +75, lng: +45};
                var markers = [];
                var mcOptions = {gridSize: 50, maxZoom: 30};
                var map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 3,
                    disableDefaultUI: true,
                    zoomControl:true,
                    center: myLatLng
                 });

                map.mapTypes.set("map_style", styledMap);
                map.setMapTypeId("map_style");
                var maxlat=0;
                var maxlong=0 ;
                var maxZoomLevel = 12 ;
                for(var i=0;i<json.length;i++) {
                    var labelIndex = i;
                    if(json[i]){
                    maxlat = maxlat + json[i].latitude ;
                    maxlong= maxlong + json[i].longitude ;
                        var myLatLng = {lat: json[i].latitude, lng: json[i].longitude};
                            var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            icon : markerimage,
                            title: json[i].city
                        });
                        markers.push(marker);
                    }
                }
                maxlat = maxlat/json.length ;
                maxlong = maxlong/json.length;
                var myLatLng = {lat: maxlat, lng: maxlong};
                
                map.setCenter(myLatLng);
                var markerCluster = new MarkerClusterer(map, markers,mcOptions);
            } else {
                var myLatLng = {lat: +30, lng: 0};
                 var map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 3,
                    disableDefaultUI: true,
                    zoomControl:true,
                    center: myLatLng
                 });
                map.mapTypes.set("map_style", styledMap);
                map.setMapTypeId("map_style");
            }

             // Limit the zoom level
             google.maps.event.addListener(map, "zoom_changed", function () {
                 if (map.getZoom() > maxZoomLevel) map.setZoom(maxZoomLevel);
             });

        },
        error : function(resultat, statut, erreur) {
		//console.log(resultat);
		//console.log(statut);
		//console.log(erreur);
        },
        complete : function(resultat, statut) {
		//console.log(resultat);
		//console.log(statut);
        }
        });
        }
        </script>
        <script async defer
          src="'.$mapurl.'">
        </script>';


    return $map;
}

   function display_group_member($groupid, $courseid){

        global $CFG, $OUTPUT ;

        $table = new html_table();

        $table->head = array(get_string('userimage','moodecgrpmanagement'),get_string('fullnameuser'),get_string('userlocation','moodecgrpmanagement') );
        $member = groups_get_members($groupid, $fields='u.*');
        $member = array_values($member);
        for($i=0;$i<count($member);$i++)
        {
              if($member[$i]->city){
                if($member[$i]->country){
                    $country = '('.$member[$i]->country.')' ;
                }else {
                    $country = '' ;
                }
            }else {
                $country = $member[$i]->country ;
            }


            $img = $OUTPUT->user_picture($member[$i], array('courseid'=>$courseid)) ;
            $table->data[]= array_values(array($img,'<a href="'.$CFG->wwwroot.'/user/view.php?id=' .$member[$i]->id.'">'.$member[$i]->firstname.' '.$member[$i]->lastname .'</a>',  $member[$i]->city .' '.$country));
        }

        return $table ;
    }
	
	function display_group_member_detail($groupid, $courseid){

        global $CFG, $OUTPUT ;

        $table = new html_table();
        $table->attributes['id']='table_user';
        $table->head = array(get_string('userimage','moodecgrpmanagement'),get_string('fullnameuser'),get_string('userlocation','moodecgrpmanagement') );
        $member = groups_get_members($groupid, $fields='u.*');
        $member = array_values($member);
        /*for($i=0;$i<count($member);$i++)
        {

            if($member[$i]->city){
                if($member[$i]->country){
                    $country = '('.$member[$i]->country.')' ;
                }else {
                    $country = '' ;
                }
            }else {
                $country = $member[$i]->country ;
            }

            $img = $OUTPUT->user_picture($member[$i], array('courseid'=>$courseid)) ;
            $table->data[]= array_values(array($img,'<a href="'.$CFG->wwwroot.'/user/view.php?id=' .$member[$i]->id.'">'.$member[$i]->firstname.' '.$member[$i]->lastname .'</a>',  $member[$i]->city .' '.$country));
        }*/

        return $member ;
    }

function display_ungroup_member_report($member, $course){

    global $CFG, $OUTPUT ;

    $table = new html_table();
    $table->head = array(get_string('userpic'),get_string('fullnameuser'));
    $member = array_values($member);
    for($i=0;$i<count($member);$i++)
    {
        $img = $OUTPUT->user_picture($member[$i], array('courseid'=>$course)) ;
        $table->data[]= array_values(array($img,'<a href="'.$CFG->wwwroot.'/user/view.php?id=' .$member[$i]->id.'">'.$member[$i]->firstname. ' '.$member[$i]->lastname.'</a>'));
    }

    return $table ;

}
function display_group_member_report($groupid, $course){

    global $CFG, $OUTPUT ;

    $table = new html_table();
    $table->head = array(get_string('userpic'),get_string('fullnameuser'));
    $member = groups_get_members($groupid, $fields='u.*');
    $member = array_values($member);
    for($i=0;$i<count($member);$i++)
    {
        $img = $OUTPUT->user_picture($member[$i], array('courseid'=>$course)) ;
        $table->data[]= array_values(array($img,'<a href="'.$CFG->wwwroot.'/user/view.php?id=' .$member[$i]->id.'">'.$member[$i]->firstname. ' '.$member[$i]->lastname.'</a>'));
    }

    return $table ;
}

function print_picture_group($group, $courseid, $large=false, $return=false) {
    global $CFG;

    if (is_array($group)) {
        $output = '';
        foreach ($group as $g) {
            $output .= print_picture_group($g, $courseid, $large, true);
        }
        if ($return) {
            return $output;
        } else {
            echo $output;
            return;
        }
    }

    $context = context_course::instance($courseid);

    // If there is no picture, do nothing.
    if (!$group->picture) {
        return '';
    }

    // If picture is hidden, only show to those with course:managegroups.
    if ($group->hidepicture and !has_capability('moodle/course:managegroups', $context)) {
        return '';
    }


    $output = '';
    if ($large) {
        $file = 'f1';
    } else {
        $file = 'f2';
    }

    $grouppictureurl = moodle_url::make_pluginfile_url($context->id, 'group', 'icon', $group->id, '/', $file);
    $grouppictureurl->param('rev', $group->picture);
    $output .= '<img class="grouppicture" src="'.$grouppictureurl.'"'.
        ' alt="'.s(get_string('group').' '.$group->name).'" title="'.s($group->name).'"/>';

   

    if ($return) {
        return $output;
    } else {
        echo $output;
    }
}