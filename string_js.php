<?php
/**
* User: Martin Tazlari  @Cyberlearn
* Date: 01.12.2015
* Time: 11:35
* @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/?>
<div>
    <input type="hidden" id="previousString" value="<?php echo get_string("previousString", "moodecgrpmanagement") ?>">
    <input type="hidden" id="nextString" value="<?php echo get_string("nextString", "moodecgrpmanagement") ?>">
    <input type="hidden" id="showString" value="<?php echo get_string("showString", "moodecgrpmanagement") ?>">
    <input type="hidden" id="pageString" value="<?php echo get_string("pageString", "moodecgrpmanagement") ?>">
    <input type="hidden" id="searchString" value="<?php echo get_string("searchString", "moodecgrpmanagement") ?>">
    <input type="hidden" id="toString" value="<?php echo get_string("toString", "moodecgrpmanagement") ?>">
    <input type="hidden" id="ofString" value="<?php echo get_string("ofString", "moodecgrpmanagement") ?>">
	<input type="hidden" id="recordString" value="<?php echo get_string("recordString", "moodecgrpmanagement") ?>">
	<input type="hidden" id="filtredFromString" value="<?php echo get_string("filtredFromString", "moodecgrpmanagement") ?>">
	<input type="hidden" id="totalRecordsString" value="<?php echo get_string("totalRecordsString", "moodecgrpmanagement") ?>">
</div>



