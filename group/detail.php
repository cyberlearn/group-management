<?php
/**
 * User: Martin Tazlari  @Cyberlearn
 * Date: 01.12.2015
 * Time: 11:35
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../../config.php');
require_once('../lib.php');
// include the php script

global $DB;
/// get url variables
$cgid     = required_param('cgid', PARAM_INT);
$cmid     = required_param('cmid', PARAM_INT);
$courseid = optional_param('courseid', 0, PARAM_INT);
$id       = optional_param('id', 0, PARAM_INT);

require_login($COURSE);

if ($id !== 0) {
    $PAGE->set_url('/mod/moodecgrpmanagement/group/detail.php', array('id'=>$id, 'cmid'=>$cmid, 'cgid'=>$cgid,'courseid'=>$courseid));
} else {
    $PAGE->set_url('/mod/moodecgrpmanagement/group/detail.php', array('courseid'=>$courseid, 'cmid'=>$cmid, 'cgid'=>$cgid));
}

$course = $DB->get_record('course',array('id'=>$courseid));
$groupmanagement = $DB->get_record('moodecgrpmanagement',array('id'=>$cgid));
$groupmanagementoptions = $DB->get_record('moodecgrpmanagement_options',array('moodecgrpmanagementid'=>$cgid, 'groupid'=>$id));
$group = $DB->get_record('groups',array('id'=>$id));
$groupcreator = $DB->get_record('user',array('id'=>$groupmanagementoptions->creatorid));
$members = groups_get_members($id, $fields='u.*');
$members = array_values($members);


$context = context_course::instance($courseid);
$PAGE->set_context($context);

$strgroups = get_string('groups');
$strheading = get_string('groupdetail', 'moodecgrpmanagement');


$PAGE->set_title($strgroups);
$PAGE->set_pagelayout('admin');


$PAGE->navbar->add($course->shortname, new moodle_url('/course/view.php', array('id'=>$courseid)));
$PAGE->navbar->add($groupmanagement->name, new moodle_url('/mod/moodecgrpmanagement/view.php', array('id'=>$cmid)));
$PAGE->navbar->add($strheading);
$PAGE->requires->css('/mod/moodecgrpmanagement/lib/jquery.dynatable.css');
/// Print header
$PAGE->requires->jquery();
$PAGE->requires->js('/mod/moodecgrpmanagement/markerclusterer.js');
include_once('../string_js.php');
$PAGE->requires->js('/mod/moodecgrpmanagement/lib/jquery.dynatable.js',true);
$PAGE->requires->js('/mod/moodecgrpmanagement/javascript.js',true);

echo $OUTPUT->header();

echo '<input type="hidden" id="groupid" value='.$id.'>' ;

if ($id) {

    echo '<div id="groupdetail">' ;

    // Title
    echo '<div id="mooc">';
    echo '<h2>'.print_picture_group($group, $course->id,$large=true,$return=true). ' ' .$group->name.' </h2>';
    echo '</div>';

    echo '<div id="descriptionGroup">';
    echo '<div id="iframevideo">';
    echo $groupmanagementoptions->groupvideo;
    echo '</div>';
    echo '<div id="descriptionSummary">';
    echo $group->description;
    echo '</div>';
    echo '<div id="clear"></div>';
    echo '</div>';

    echo '<div id="groupMemberInfo">';
    // Group image


    echo '<div class="title">'. get_string('grpmembernbr','moodecgrpmanagement').'</div>';
    echo '<div class="titleNbr">'.count($members).'</div>';

    echo '<div id="groupCountry">';
    $memberflag = array() ;
    $membernocountry = array();
    $membernocountry["nocountry"] = 0 ;
    foreach($members as $member)
    {
        if($member->country){

            if(!isset($memberflag[$member->country])) {
                $memberflag[$member->country] = 0 ;
            }
            $memberflag[$member->country]++;
        }else {
            $membernocountry["nocountry"]++;
        }
    }

    foreach($memberflag as $key=>$value)
    {
        $key = strtolower($key);
        $flagclass = "flag flag-".$key ;
        echo '<div class="countryBlock">';
        echo '<div class="'.$flagclass.'"></div>';
        echo '<div class="txt">'.$value.'</div>';
        echo '</div>';
    }
    if($membernocountry["nocountry"]>0){
        echo '<div class="countryBlock">';
        echo '<div> <img class="flag" src="../pix/unknowcountry.png" /></div>';
        echo '<div class="txt">'.$membernocountry["nocountry"].'</div>';
        echo '</div>';
    }

    echo '</div>';


    // Group Creator

    if($groupcreator){
        echo '<div class="title prof">';
        echo get_string('groupcreator','moodecgrpmanagement');
        echo '</div>';
        echo '<div class="imgProf">';
        echo $OUTPUT->user_picture($groupcreator, array('size'=>1));
        echo '</div>';
        echo '<div class="txtProf"><h4>';
        echo $groupcreator->firstname . ' '. $groupcreator->lastname ;
        echo '</h4></div>';

    }
    echo '</div>';


    echo '<div id="clear"></div>';
    // Info group

    echo '<div id="locationInfo">';

    // Display Group Map
    $html = display_group_map($id);
    echo($html);
    echo '</br>';
    echo '<h2>'. get_string('groupmembers').'</h2>';
    echo '</br>';
    echo '<div id="groupmember">';


    // Display Group Member
    $groupmember = display_group_member_detail($id,$courseid);
    $countUser = count($groupmember);
    echo '<table id="table_user" class="general table" cellspacing="0" width="100%">';
    echo '<thead><tr><th data-dynatable-no-sort="true">'.get_string("userimage","moodecgrpmanagement").'</th><th>'.get_string("fullnameuser").'</th><th>'.get_string("userlocation","moodecgrpmanagement").'</th></thead>';
    echo '<tbody>';

    for($i = 0 ; $i < $countUser ; $i++){

        $img = $OUTPUT->user_picture($groupmember[$i], array('courseid'=>$courseid)) ;
        $fullname = '<a href="'.$CFG->wwwroot.'/user/view.php?id=' .$groupmember[$i]->id.'">'.$groupmember[$i]->firstname.' '.$groupmember[$i]->lastname .'</a>';

        if($groupmember[$i]->city){
            if($groupmember[$i]->country){
                $country = '('.$groupmember[$i]->country.')' ;
            }else {
                $country = '' ;
            }
        }else {
            $country = $groupmember[$i]->country ;
        }


        $origin = $groupmember[$i]->city .' '.$country;

        echo '<tr><td>'.$img.'</td><td>'.$fullname.'</td><td>'.$origin.'</td></tr>';
    }


    echo '</tbody>';
    echo '</table>';


    echo '</div>';
    echo '</div>';
}
echo '</div>';
echo $OUTPUT->footer();
