// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package    mod
 * @subpackage moodecgrpmanagement
 * @copyright  2013 Université de Lausanne
 * @author     Nicolas Dunand <Nicolas.Dunand@unil.ch>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @modifed by Martin Tazlari
 * @copyright 2016 Cyberlearn
 */
$(function(){
    $("#table_user").dynatable();
	$('#savemychoice').val(countcheckbox()) ;

	$('#grouptabledisplay').find(':checkbox').change( function() {
		$('#savemychoice').val(countcheckbox()) ;		
	});	
	
	$('#grouptabledisplay').find(':radio').change( function() {
		$('#savemychoice').val(countcheckbox()) ;		
	});	
	
});

function countcheckbox(){
			
			var numberOfChecked = $('input:checkbox:checked').length;
			var numberOfRadio = $('input:radio:checked').length;

			var valueToCompare = 0 ;
			if(numberOfChecked > numberOfRadio) {
				valueToCompare = numberOfChecked ;
			} else {
				valueToCompare = numberOfRadio ;
			}
			
			var savemychoicestring = '' ;
				switch(valueToCompare){
					case 0 :
					savemychoicestring = $('#nogroupbuttonstring').val();
					break ;
					case 1 : 
					savemychoicestring = $('#onegroupbuttonstring').val();
					break ;
					default : 
					savemychoicestring = $('#multiplegroupbuttonstring').val();
				}			
			return savemychoicestring ;
			
} 		


var NDY = YUI().use("node", function(Y) {
    var moodecgrpmanagement_memberdisplay_click = function(e) {

        var names = Y.all('div.moodecgrpmanagements-membersnames'),
            btnShowHide = Y.all('a.moodecgrpmanagement-memberdisplay');

        btnShowHide.toggleClass('hidden');
        names.toggleClass('hidden');

        e.preventDefault();

    };
    Y.on("click", moodecgrpmanagement_memberdisplay_click, "a.moodecgrpmanagement-memberdisplay");

    var moodecgrpmanagement_descriptiondisplay_click = function(e) {

        var names = Y.all('div.moodecgrpmanagements-descriptions'),
            btnShowHide = Y.all('a.moodecgrpmanagement-descriptiondisplay');

        btnShowHide.toggleClass('hidden');
        names.toggleClass('hidden');

        e.preventDefault();

    };
    Y.on("click", moodecgrpmanagement_descriptiondisplay_click, "a.moodecgrpmanagement-descriptiondisplay");
    Y.delegate('click', function() { Y.one("table.moodecgrpmanagements~input[type='submit'][class='button']").hide(); },  Y.config.doc, "table.moodecgrpmanagements input[id^='choiceid_'][type='radio'][checked]", this);
    Y.delegate('click', function() { Y.one("table.moodecgrpmanagements~input[type='submit'][class='button']").show(); },  Y.config.doc, "table.moodecgrpmanagements input[id^='choiceid_'][type='radio']:not([checked])", this);
});
