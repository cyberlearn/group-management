<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package    mod
 * @subpackage moodecgrpmanagement
 * @copyright  2013 Université de Lausanne
 * @author     Nicolas Dunand <Nicolas.Dunand@unil.ch>
 * @modifed by Martin Tazlari
 * @copyright 2016 Cyberlearn
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

define ('moodecgrpmanagement_DISPLAY_HORIZONTAL_LAYOUT', 0);
define ('moodecgrpmanagement_DISPLAY_VERTICAL_LAYOUT', 1);

class mod_moodecgrpmanagement_renderer extends plugin_renderer_base {

    /**
     * Returns HTML to display moodecgrpmanagements of option
     * @param object $options
     * @param int  $coursemoduleid
     * @param bool $vertical
     * @return string
     */
    public function display_options($options, $coursemoduleid, $vertical = true, $publish = false, $limitmaxusersingroups = false, $showresults = false, $current = false, $moodecgrpmanagementopen = false, $disabled = false, $multipleenrollmentspossible = false) {
        global $CFG, $DB, $PAGE, $USER, $OUTPUT, $course, $moodecgrpmanagement_groups, $moodecgrpmanagement_users, $moodecgrpmanagement, $context;
        
		$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/moodecgrpmanagement/javascript.js'),true);
		
        $layoutclass = 'vertical';
        $target = new moodle_url('/mod/moodecgrpmanagement/view.php');
        $attributes = array('method'=>'POST', 'action'=>$target, 'class'=> $layoutclass.' hasScrollX');
        $html = '';
		$html .= html_writer::empty_tag('input', array('id'=>'nogroupbuttonstring','type'=>'hidden','value'=> get_string('nogroupbtnname','moodecgrpmanagement')));
		$html .= html_writer::empty_tag('input', array('id'=>'onegroupbuttonstring','type'=>'hidden','value'=>get_string('onegroupbtnname','moodecgrpmanagement')));
		$html .= html_writer::empty_tag('input', array('id'=>'multiplegroupbuttonstring','type'=>'hidden','value'=>get_string('multiplegroupbtnname','moodecgrpmanagement')));
        $html .= html_writer::start_tag('form', $attributes);
        $html .= html_writer::start_tag('table', array('id'=>'grouptabledisplay','class'=>'generaltable boxaligncenter'));
				$html .= html_writer::start_tag('thead');
        $html .= html_writer::start_tag('tr', array('class'=>'cell c0'));
        //$html .= html_writer::tag('th', get_string('choice', 'moodecgrpmanagement'), array('style'=>'width: 50px;'));
				$html .= html_writer::tag('th','', array('id'=>'choix'));
        $group = get_string('group');

        //$group .= html_writer::tag('a', get_string('showdescription', 'moodecgrpmanagement'), array('class' => 'moodecgrpmanagement-descriptiondisplay moodecgrpmanagement-descriptionshow', 'href' => '#'));
        //$group .= html_writer::tag('a', get_string('hidedescription', 'moodecgrpmanagement'), array('class' => 'moodecgrpmanagement-descriptiondisplay moodecgrpmanagement-descriptionhide hidden', 'href' => '#'));
        $html .= html_writer::tag('th', $group);


        if ( $showresults == moodecgrpmanagement_SHOWRESULTS_ALWAYS or
            ($showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER and $current) or
            ($showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE and !$moodecgrpmanagementopen)) {
            

            if (!empty($moodecgrpmanagement->privategroupspossible) && $moodecgrpmanagement->privategroupspossible == 1) {
                //$html .= html_writer::tag('th', get_string('private', 'moodecgrpmanagement'), array('style'=>''));
            }

            $html .= html_writer::tag('th', get_string('groupcreator', 'moodecgrpmanagement'));

            //if ($publish == moodecgrpmanagement_PUBLISH_NAMES) {

                //$membersdisplay_html = html_writer::tag('a', get_string('show'), array('class' => 'moodecgrpmanagement-memberdisplay moodecgrpmanagement-membershow', 'href' => '#'));
                //$membersdisplay_html .= html_writer::tag('a', get_string('hide'), array('class' => 'moodecgrpmanagement-memberdisplay moodecgrpmanagement-memberhide hidden', 'href' => '#'));

               // $html .= html_writer::tag('th', get_string('groupmembers', 'moodecgrpmanagement') .'</br>'. $membersdisplay_html);

            //}
		  // $html .= html_writer::tag('th', '');
            $html .= html_writer::tag('th', get_string('edit'), array('style'=>'min-width: 85px;'));
        }

        $html .= html_writer::end_tag('tr');
				$html .= html_writer::end_tag('thead');
				
				
        $availableoption = count($options['options']);
        if ($multipleenrollmentspossible == 1) {
            $i=0;
            $answer_to_groupid_mappings = '';
        }

        $initiallyHideSubmitButton = false;
        $private_groups_id = array();

        $disableDeletion = false;
        if(count($options['options']) <= 2) {
            $disableDeletion = true;
        }
				$hasEnrollKey = false;
        foreach ($options['options'] as $option) {


            // select la bonne clé
            $param = array($option->groupid);
            $query = "SELECT enrolmentkey FROM {groups} WHERE id= ?";
            $enrolmentkey = $DB->get_field_sql($query,$param);



            if(isset($enrolmentkey)&& $enrolmentkey!=""){
                $option->enrollementkey = $enrolmentkey;
            }else {
                $option->enrollementkey = null ;
            }




            $group = (isset($moodecgrpmanagement_groups[$option->groupid])) ? ($moodecgrpmanagement_groups[$option->groupid]) : (false);



                if (!$group) {
                    $colspan = 2;
                    if ($showresults == moodecgrpmanagement_SHOWRESULTS_ALWAYS or ($showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER and $current) or ($showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE and !$moodecgrpmanagementopen)) {
                        $colspan++;
                        if ($publish == moodecgrpmanagement_PUBLISH_NAMES) {
                            $colspan++;
                        }
                    }
                   // $cell = html_writer::tag('td', get_string('groupdoesntexist', 'moodecgrpmanagement'), array('colspan' => $colspan));
                   // $html .= html_writer::tag('tr', $cell);
                    break;
                }
                $html .= html_writer::start_tag('tr', array('class' => 'option'));
                $html .= html_writer::start_tag('td', array('style'=>'vertical-align: middle;'));
				// Enrolment change
                if ($multipleenrollmentspossible == 1) {
                    $option->attributes->name = 'answer_' . $i;
                    $option->attributes->type = 'checkbox';
                    $answer_to_groupid_mappings .= '<input type="hidden" name="answer_' . $i . '_groupid" value="' . $option->groupid . '">';
                    $option->attributes->onchange = 'if ($("#enrollementKeyTd" + this.value).css("display") == "none" && $(this).is(":checked")) {
                                                    $("#enrollementKeyTd" + this.value).show();
                                                 } else {
                                                    $("#enrollementKeyTd" + this.value).hide();
                                                 }';
											
                    $i++;
                } else {
                    $option->attributes->name = 'answer';
                    $option->attributes->type = 'radio';
                    $option->attributes->onchange = '$(".enrollementKey").hide();
													 $("#enrollementKeyTd" + this.value).show();';
                    if (array_key_exists('attributes', $option) && array_key_exists('checked', $option->attributes) && $option->attributes->checked == true) {
                        $initiallyHideSubmitButton = true;
                    }
                }

                $group_title = "";
				$urldetail = new moodle_url('/mod/moodecgrpmanagement/group/detail.php', array('id' => $option->groupid, 'courseid' => $course->id, 'cgid' => $moodecgrpmanagement->id, 'cmid' => $coursemoduleid));
                $group_title .= '<a href='.$urldetail.'>';

                if ($moodecgrpmanagement->displaygrouppicture == 1 && $group->picture!=0 ) {
                    $group_title .=  print_picture_group($group, $course->id,false,$return=true);
                }

                $group_title .= '' . $group->name . '';
                $group_title .= '</a>';

                $labeltext = html_writer::tag('label', $group_title, array('class'=>'nameGroup', 'for' => 'choiceid_' . $option->attributes->value));

                $group_members = $DB->get_records_sql('SELECT {user}.id
                                                   FROM {user}
                                                   RIGHT OUTER JOIN {groups_members} ON {user}.id = {groups_members}.userid
                                                   WHERE groupid = ?', array($group->id));

                if (!empty($option->attributes->disabled) || ($limitmaxusersingroups && sizeof($group_members) >= $option->maxanswers)) {
                    $labeltext .= ' ' . html_writer::tag('em', get_string('full', 'moodecgrpmanagement'));
                 //   $option->attributes->disabled = true;
                    $availableoption--;
                }

                if ($moodecgrpmanagement->freezegroups == 1 || (!empty($moodecgrpmanagement->freezegroupsaftertime) && time() >= $moodecgrpmanagement->freezegroupsaftertime)) {
                    $option->attributes->disabled = true;
                }

                $group_description = $group->description;

                $labeltext .= html_writer::tag('div', $group_description, array('class' => 'moodecgrpmanagements-descriptions hidden'));

                if ($moodecgrpmanagement->displaygroupvideo == 1) {

                    if (isset($option->groupvideo)) {
                        $videoEmbed = $option->groupvideo; // Create and add iframe
                        $labeltext .= html_writer::tag('div', $videoEmbed, array('class' => 'moodecgrpmanagements-descriptions hidden'));
                    }
                }

                if ($disabled) {
                    $option->attributes->disabled = true;
                }
                $attributes = (array)$option->attributes;
                $attributes['id'] = 'choiceid_' . $option->attributes->value;
                $html .= html_writer::empty_tag('input', $attributes);
                $html .= html_writer::end_tag('td');
								
								
								
								
                $html .= html_writer::start_tag('td', array('for' => $option->attributes->name));
								$html .= $labeltext;
                if ($showresults == moodecgrpmanagement_SHOWRESULTS_ALWAYS or
                    ($showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER and $current) or
                    ($showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE and !$moodecgrpmanagementopen)
                )
								{
									$txtMember = '';	
									if ($limitmaxusersingroups) {
											$txtMember = get_string('members/max', 'moodecgrpmanagement');
									} else {
											$txtMember = get_string('members/', 'moodecgrpmanagement');
									}
									
									
									
									$maxanswers = ($limitmaxusersingroups) ? (' / ' . $option->maxanswers) : ('');
                  
									$html .= html_writer::start_tag('div', array('class' => 'nbrMemberBGC', 'title'=>sizeof($group_members).' '.$txtMember));
									$html .= html_writer::start_tag('div', array('class' => 'nbrMember'));
									$html .=  sizeof($group_members) . $maxanswers;
									$html .= html_writer::end_tag('div');
									$html .= html_writer::end_tag('div');
									
									
									
									if (!empty($moodecgrpmanagement->privategroupspossible) && $moodecgrpmanagement->privategroupspossible == 1) 
									{
											$privateImage = '';
											if (isset($option->enrollementkey)) {
													$privateImage = '<img class="smallicon" src="' . $this->output->image_url('t/locked') . '" alt="' . get_string('private', 'moodecgrpmanagement') . '" title="' . get_string('private', 'moodecgrpmanagement') . '" />';
													$private_groups_id[] = $option->attributes->value;
													$hasEnrollKey = true;
											}
											//$html .= html_writer::tag('td', $privateImage, array('style' => 'text-align:center'));
											$html .= html_writer::start_tag('div', array('class' => 'iconPrivate'));
											$html .= $privateImage;
											$html .= html_writer::end_tag('div');
									}
									else
									{
										$hasEnrollKey = false;
									}
									
									$html .= html_writer::start_tag('div', array('style' => 'clear:both'));
									$html .= html_writer::end_tag('td');
                  $html .= html_writer::end_tag('td');  

										
										

                    

                    $group_creator_links = '-';
                    if (isset($option->creatorid)) {
                        $group_creator = $DB->get_record('user', array('id' => $option->creatorid));
                        if (isset($group_creator)) {
                            $url = new moodle_url('/message/index.php', array('id' => $option->creatorid, 'course' => $course->id));
                            $creatorImage = '<img class="smallicon" src="' . $this->output->image_url('t/email') . '" alt="' . get_string('contact', 'moodecgrpmanagement') . '" title="' . get_string('contact', 'moodecgrpmanagement') . '" />';
                            $group_creator_links = html_writer::link($url, $creatorImage);
                            $group_creator_links .= ' ';
                            $url = new moodle_url('/user/view.php', array('id' => $option->creatorid, 'course' => $course->id));
                            $group_creator_name = $group_creator->lastname . ', ' . $group_creator->firstname;
                            $group_creator_links .= html_writer::link($url, $group_creator_name);
                        }
                    }
                    $html .= html_writer::tag('td', $group_creator_links);

                    if ($publish == moodecgrpmanagement_PUBLISH_NAMES) 
										{
                        $group_member_html = '';
                        foreach ($group_members as $group_member) {
                            $url = new moodle_url('/user/view.php', array('id' => $group_member->id, 'course' => $course->id));
                            $group_member_link = html_writer::link($url,'<br />');
                            $group_member_html .= html_writer::tag('div', $group_member_link, array('class' => 'moodecgrpmanagements-membersnames hidden', 'id' => 'moodecgrpmanagement_' . $option->attributes->value));
                        }
                        if (empty($group_member_html)) {
                            $group_member_html = html_writer::tag('div', '-', array('class' => 'moodecgrpmanagements-membersnames hidden', 'id' => 'moodecgrpmanagement_' . $option->attributes->value));
                        }
					  //$html .= html_writer::tag('td', $group_member_html, array('class' => 'center'));
                    }

                    $actionLinks = '';
                    $url = new moodle_url('/mod/moodecgrpmanagement/group/detail.php', array('id' => $option->groupid, 'courseid' => $course->id, 'cgid' => $moodecgrpmanagement->id, 'cmid' => $coursemoduleid));
                    $editImage = '<img class="smallicon" src="' . $this->output->image_url('t/cohort') . '" alt="' . get_string('info', 'moodle') . '" title="' . get_string('info', 'moodle') . '" />';
                    $actionLinks .= html_writer::link($url, $editImage, array('style' => 'padding:0 3px'));


                    $hasManageGroupsCapability = has_capability('mod/moodecgrpmanagement:managegroups', $context);
                    if ($hasManageGroupsCapability || (isset($option->creatorid) && $option->creatorid == $USER->id)) {
                        if ($moodecgrpmanagement->freezegroups == 0 && (empty($moodecgrpmanagement->freezegroupsaftertime) || time() < $moodecgrpmanagement->freezegroupsaftertime)) {
                            $url = new moodle_url('/mod/moodecgrpmanagement/group/group.php', array('id' => $option->groupid, 'courseid' => $course->id, 'cgid' => $moodecgrpmanagement->id, 'cmid' => $coursemoduleid));
                            $editImage = '<img class="smallicon" src="' . $this->output->image_url('t/edit') . '" alt="' . get_string('edit', 'moodle') . '" title="' . get_string('edit', 'moodle') . '" />';
                            $actionLinks .= html_writer::link($url, $editImage, array('style' => 'padding:0 3px'));




                            if (!$disableDeletion) {
                                $url = new moodle_url('/mod/moodecgrpmanagement/group/delete.php', array('groups' => $option->groupid, 'courseid' => $course->id, 'cmid' => $coursemoduleid));
                                $deleteImage = '<img class="smallicon" src="' . $this->output->image_url('t/delete') . '" alt="' . get_string('delete', 'moodle') . '" title="' . get_string('delete', 'moodle') . '" />';
                                $actionLinks .= html_writer::link($url, $deleteImage, array('style' => 'padding:0 3px'));

                            }
                        }
                    }
                    $html .= html_writer::tag('td', $actionLinks, array('style' => 'text-align:center; vertical-align: middle;'));
                }
								else
								{
									$html .= html_writer::end_tag('td');  
								}
                $html .= html_writer::end_tag('tr');
								
								if($hasEnrollKey)
								{
									 $html .= html_writer::start_tag('tr');
									 $contenuKey = get_string('enrolmentkey', 'group').': ';
									 $contenuKey .= html_writer::empty_tag('input', array('type' => 'password', 'name' => 'enrollementKeyKey' . $option->attributes->value, 'id' => 'enrollementKeyKey' . $option->attributes->value, 'class' => 'enrollementKey', 'style' => 'margin: 0;'));
									 $url = new moodle_url('/message/index.php', array('id' => $option->creatorid, 'course' => $course->id));
                  
									 $contenuKey .= ' '.html_writer::link($url, get_string('requestEnrollementKeyFromShort', 'moodecgrpmanagement'));
									 
									 
									 $html .= html_writer::tag('td', $contenuKey, array('id'=>'enrollementKeyTd'.$option->attributes->value, 'colspan'=>'6',  'style' => 'text-align:left; display: none; background-color:rgba(200, 200, 200, 0.4)'));
									 
									 
									 
									 

									 
									 
									 $html .= html_writer::end_tag('tr');
									 $html .= html_writer::start_tag('tr');
									 $html .= html_writer::tag('td', '', array('colspan'=>'6',  'style' => ' height: 1px; display: none'));
									 $html .= html_writer::end_tag('tr');
									 $hasEnrollKey = false;
								}

        }
				

        $html .= html_writer::end_tag('table');

				$html .= html_writer::start_tag('div', array( 'class'=>'form-buttons') );
				



        if ($multipleenrollmentspossible == 1) {
            $html .= '<input type="hidden" name="number_of_groups" value="' . $i . '">' . $answer_to_groupid_mappings;
        }
        //$html .= html_writer::tag('div', '', array('class' => 'clearfloat'));
        $html .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'sesskey', 'value' => sesskey()));
        $html .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'id', 'value' => $coursemoduleid));

        if (!empty($options['hascapability']) && ($options['hascapability'])) {
            if ($availableoption < 1) {
                //$html .= html_writer::tag('div', get_string('moodecgrpmanagementfull', 'moodecgrpmanagement'));
            } else {
                if (!$disabled) {
									/*
                    foreach ($private_groups_id as $private_group_id) {
                        $moodecgrpmanagement_option = $DB->get_record('moodecgrpmanagement_options', array('id' => $private_group_id));
                        $groupName = $moodecgrpmanagement_groups[$moodecgrpmanagement_option->groupid]->name;
                        $labelText = '<b>' . get_string('enrollementKeyFormoodecgrpmanagement', 'moodecgrpmanagement') . ' ' . $groupName . '</b><br />';

                        if (!empty($moodecgrpmanagement_option->creatorid)) {
                            $group_creator_links = '';
                            $group_creator = $DB->get_record('user', array('id' => $moodecgrpmanagement_option->creatorid));
                            if (isset($group_creator)) {
                                $url = new moodle_url('/message/index.php', array('id' => $option->creatorid, 'course' => $course->id));
                                $group_creator_name = $group_creator->firstname . ' ' . $group_creator->lastname;
                                $group_creator_links .= html_writer::link($url, $group_creator_name);
                            }

                            $labelText .= get_string('requestEnrollementKeyFrom', 'moodecgrpmanagement') . ' ' . $group_creator_links . '<br />';
                        }

                        $html .= html_writer::tag('label', $labelText, array('for' => 'enrollementKeyKey' . $private_group_id, 'id' => 'enrollementKeyLabel' . $private_group_id, 'class' => 'enrollementKey', 'style' => 'display: none;'));
                        $html .= html_writer::empty_tag('input', array('type' => 'password', 'name' => 'enrollementKeyKey' . $private_group_id, 'id' => 'enrollementKeyKey' . $private_group_id, 'class' => 'enrollementKey', 'style' => 'display: none;'));
                    }
										
										*/
                    if ($moodecgrpmanagement->freezegroups == 0 && (empty($moodecgrpmanagement->freezegroupsaftertime) || time() < $moodecgrpmanagement->freezegroupsaftertime)) {
                        $html .= html_writer::empty_tag('input', array('id'=>'savemychoice','type' => 'submit', 'value' => get_string('savemymoodecgrpmanagement', 'moodecgrpmanagement'), 'class' => 'button', 'style' => $initiallyHideSubmitButton ? 'display: none' : ''));
                    }
                }
            }

            if (!empty($options['allowupdate']) && ($options['allowupdate']) && !($multipleenrollmentspossible == 1)) {
                if ($moodecgrpmanagement->freezegroups == 0 && (empty($moodecgrpmanagement->freezegroupsaftertime) || time() < $moodecgrpmanagement->freezegroupsaftertime)) {
                    $url = new moodle_url('view.php', array('id' => $coursemoduleid, 'action' => 'delmoodecgrpmanagement', 'sesskey' => sesskey()));

                    $html .= ' ' . html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('removemymoodecgrpmanagement', 'moodecgrpmanagement'), 'class' => 'button', 'onclick' => 'window.location="' . html_entity_decode($url) . '"'));

                }
            }
        } else {
            $html .= html_writer::tag('div', get_string('havetologin', 'moodecgrpmanagement'));
        }
        // Check if can create group
        if ($moodecgrpmanagement->groupcreationpossible == 1) {
            if ($moodecgrpmanagement->freezegroups == 0 && (empty($moodecgrpmanagement->freezegroupsaftertime) || time() < $moodecgrpmanagement->freezegroupsaftertime)) {


                if ($moodecgrpmanagement->limitmaxgroups == 0 || count($options['options']) < $moodecgrpmanagement->maxgroups) {
                    $url = new moodle_url('/mod/moodecgrpmanagement/group/group.php', array('cgid' => $moodecgrpmanagement->id, 'cmid' => $coursemoduleid, 'courseid' => $course->id));
                    $html .= '' . html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('creategroup', 'moodecgrpmanagement'), 'class' => 'button', 'onclick' => 'window.location="' . html_entity_decode($url) . '"'));
                }
            }
        } else {

            // If not, check if is a teacher, non editing teacher or admin and let create group
            if (has_capability('mod/moodecgrpmanagement:managegroups', $context)) {
                if ($moodecgrpmanagement->freezegroups == 0 && (empty($moodecgrpmanagement->freezegroupsaftertime) || time() < $moodecgrpmanagement->freezegroupsaftertime)) {
                    if ($moodecgrpmanagement->limitmaxgroups == 0 || count($options['options']) < $moodecgrpmanagement->maxgroups) {
                        $url = new moodle_url('/mod/moodecgrpmanagement/group/group.php', array('cgid' => $moodecgrpmanagement->id, 'cmid' => $coursemoduleid, 'courseid' => $course->id));
                        $html .= '<br/>' . html_writer::empty_tag('input', array('type' => 'button', 'value' => get_string('creategroup', 'moodecgrpmanagement'), 'class' => 'button', 'onclick' => 'window.location="' . html_entity_decode($url) . '"'));
                    }
                }
            }
        }

				$html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('form');
	        return $html;

    }

    /**
     * Returns HTML to display moodecgrpmanagements result
     * @param object $moodecgrpmanagements
     * @param bool $forcepublish
     * @return string
     */
    public function display_result($moodecgrpmanagements, $forcepublish = false) {
        if (empty($forcepublish)) { //allow the publish setting to be overridden
            $forcepublish = $moodecgrpmanagements->publish;
        }

        $displaylayout = ($moodecgrpmanagements) ? ($moodecgrpmanagements->display) : (moodecgrpmanagement_DISPLAY_HORIZONTAL);

        if ($forcepublish) {  //moodecgrpmanagement_PUBLISH_NAMES
            return $this->display_publish_name_vertical($moodecgrpmanagements);
        } else { //moodecgrpmanagement_PUBLISH_ANONYMOUS';
            if ($displaylayout == moodecgrpmanagement_DISPLAY_HORIZONTAL_LAYOUT) {
                return $this->display_publish_anonymous_horizontal($moodecgrpmanagements);
            }
            return $this->display_publish_anonymous_vertical($moodecgrpmanagements);
        }
    }

    /**
     * Returns HTML to display moodecgrpmanagements result
     * @param object $moodecgrpmanagements
     * @param bool $forcepublish
     * @return string
     */
    public function display_publish_name_vertical($moodecgrpmanagements) {
        global $PAGE;
        global $DB;
        global $context;

        if (!has_capability('mod/moodecgrpmanagement:downloadresponses', $context)) {
            return; // only the (editing)teacher can see the diagram
        }
        if (!$moodecgrpmanagements) {
            return; // no answers yet, so don't bother
        }

        $html ='';
        $html .= html_writer::tag('h2',format_string(get_string("responses", "moodecgrpmanagement")), array('class'=>'main'));

        $attributes = array('method'=>'POST');
        $attributes['action'] = new moodle_url($PAGE->url);
        $attributes['id'] = 'attemptsform';

        if ($moodecgrpmanagements->viewresponsecapability) {
            $html .= html_writer::start_tag('form', $attributes);
            $html .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'id', 'value'=> $moodecgrpmanagements->coursemoduleid));
            $html .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'sesskey', 'value'=> sesskey()));
            $html .= html_writer::empty_tag('input', array('type'=>'hidden', 'name'=>'mode', 'value'=>'overview'));
        }

        $table = new html_table();
        $table->cellpadding = 0;
        $table->cellspacing = 0;
        $table->attributes['class'] = 'generaltable boxaligncenter ';
        $table->tablealign = 'center';
        $table->data = array();

        $count = 0;
        ksort($moodecgrpmanagements->options);

        $columns = array();
        foreach ($moodecgrpmanagements->options as $optionid => $options) {
            $coldata = '';
            if ($moodecgrpmanagements->showunanswered && $optionid == 0) {
                $coldata .= format_string(get_string('notanswered', 'moodecgrpmanagement'));
            } else if ($optionid > 0) {
                $coldata .= format_string(moodecgrpmanagement_get_option_text($moodecgrpmanagements, $moodecgrpmanagements->options[$optionid]->groupid));
            }
            $numberofuser = 0;
            if (!empty($options->user) && count($options->user) > 0) {
                $numberofuser = count($options->user);
            }
            $coldata .=' ('.$numberofuser. ')' ;
            $coldata = html_writer::tag('div', $coldata);
            $columns[] = $coldata;
        }

        $table->head = $columns;

        $coldata = '';
        $columns = array();
        foreach ($moodecgrpmanagements->options as $optionid => $options) {
            $coldata = '';
            if ($moodecgrpmanagements->showunanswered || $optionid > 0) {
                if (!empty($options->user)) {
                    foreach ($options->user as $user) {
                        $data = html_writer::start_tag('div');
                        if (empty($user->imagealt)){
                            $user->imagealt = '';
                        }

                        if ($moodecgrpmanagements->viewresponsecapability && $moodecgrpmanagements->deleterepsonsecapability  && $optionid > 0) {
                            $attemptaction = html_writer::checkbox('userid[]', $user->id,'');
                            $data .= $attemptaction;
                        }
                        $userimage = $this->output->user_picture($user, array('courseid'=>$moodecgrpmanagements->courseid));
                        $data .= ' '.$userimage ;

                        $userlink = new moodle_url('/user/view.php', array('id'=>$user->id,'course'=>$moodecgrpmanagements->courseid));
                        $name = html_writer::tag('a', fullname($user, $moodecgrpmanagements->fullnamecapability), array('href'=>$userlink));
                        $data .= ' '.$name;
                        $data .= html_writer::end_tag('div');
                        $coldata .= html_writer::tag('div', $data);
                    }
                }
            }

            $columns[] = $coldata;
            $count++;
        }

        $table->data[] = $columns;
        foreach ($columns as $d) {
            $table->colclasses[] = 'data';
        }
        $html .= html_writer::tag('div', html_writer::table($table));

        $actiondata = '';
        if ($moodecgrpmanagements->viewresponsecapability && $moodecgrpmanagements->deleterepsonsecapability) {
            $selecturl = new moodle_url('#');

            $selectallactions = new component_action('click',"checkall");
            $selectall = new action_link($selecturl, get_string('selectall'), $selectallactions);
            $actiondata .= $this->output->render($selectall) . ' / ';

            $deselectallactions = new component_action('click',"checknone");
            $deselectall = new action_link($selecturl, get_string('deselectall'), $deselectallactions);
            $actiondata .= $this->output->render($deselectall);

            $actiondata .= html_writer::tag('label', ' ' . get_string('withselected', 'choice') . ' ', array('for'=>'menuaction'));

            $actionurl = new moodle_url($PAGE->url, array('sesskey'=>sesskey(), 'action'=>'delete_confirmation()'));
            $select = new single_select($actionurl, 'action', array('delete'=>get_string('delete')), null, array(''=>get_string('chooseaction', 'moodecgrpmanagement')), 'attemptsform');

            $actiondata .= $this->output->render($select);
        }


        $html .= html_writer::tag('div', $actiondata, array('class'=>'footeraction'));

        if ($moodecgrpmanagements->viewresponsecapability) {
            $html .= html_writer::end_tag('form');
        }

        return $html;
    }


    /**
     * Returns HTML to display moodecgrpmanagements result
     * @param object $moodecgrpmanagements
     * @return string
     */
    public function display_publish_anonymous_horizontal($moodecgrpmanagements) {
        global $context, $DB, $moodecgrpmanagement_COLUMN_WIDTH;

        if (!has_capability('mod/moodecgrpmanagement:downloadresponses', $context)) {
            return; // only the (editing)teacher can see the diagram
        }

        $table = new html_table();
        $table->cellpadding = 5;
        $table->cellspacing = 0;
        $table->attributes['class'] = 'results anonymous ';
        $table->data = array();

        $count = 0;
        ksort($moodecgrpmanagements->options);

        $rows = array();
        foreach ($moodecgrpmanagements->options as $optionid => $options) {
            $numberofuser = 0;
            $graphcell = new html_table_cell();
            if (!empty($options->user)) {
                $numberofuser = count($options->user);
            }

            $width = 0;
            $percentageamount = 0;
            $columndata = '';
            if($moodecgrpmanagements->numberofuser > 0) {
                $width = ($moodecgrpmanagement_COLUMN_WIDTH * ((float)$numberofuser / (float)$moodecgrpmanagements->numberofuser));
                $percentageamount = ((float)$numberofuser/(float)$moodecgrpmanagements->numberofuser)*100.0;
            }
            $displaydiagram = html_writer::tag('img','', array('style'=>'height:50px; width:'.$width.'px', 'alt'=>'', 'src'=>$this->output->image_url('row', 'moodecgrpmanagement')));

            $skiplink = html_writer::tag('a', get_string('skipresultgraph', 'moodecgrpmanagement'), array('href'=>'#skipresultgraph'. $optionid));
            $skiphandler = html_writer::tag('span', '', array('id'=>'skipresultgraph'.$optionid));

            $graphcell->text = $skiplink . $displaydiagram . $skiphandler;
            $graphcell->attributes = array('class'=>'graph horizontal');

            $datacell = new html_table_cell();
            if ($moodecgrpmanagements->showunanswered && $optionid == 0) {
                $columndata .= html_writer::tag('div', format_string(get_string('notanswered', 'moodecgrpmanagement')));
            } else if ($optionid > 0) {
                $columndata .= html_writer::tag('div', format_string(moodecgrpmanagement_get_option_text($moodecgrpmanagements, $moodecgrpmanagements->options[$optionid]->groupid)));
            }
            $columndata .= html_writer::tag('div', ' ('.$numberofuser.')', array('title'=> get_string('numberofuser', 'moodecgrpmanagement')));

            if($moodecgrpmanagements->numberofuser > 0) {
                $percentageamount = ((float)$numberofuser/(float)$moodecgrpmanagements->numberofuser)*100.0;
            }
            $columndata .= html_writer::tag('div', format_float($percentageamount,1). '%');

            $datacell->text = $columndata;
            $datacell->attributes = array('class'=>'header');

            $row = new html_table_row();
            $row->cells = array($datacell, $graphcell);
            $rows[] = $row;
        }

        $table->data = $rows;

        $html = '';
        $header = html_writer::tag('h2',format_string(get_string("responses", "moodecgrpmanagement")));
        $html .= html_writer::tag('div', $header, array('class'=>'responseheader'));
        $html .= html_writer::table($table);

        return $html;
    }

}

