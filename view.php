<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package    mod
 * @subpackage moodecgrpmanagement
 * @copyright  2013 Université de Lausanne
 * @author     Nicolas Dunand <Nicolas.Dunand@unil.ch>
 * @modifed by Martin Tazlari
 * @copyright 2016 Cyberlearn
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("lib.php");
require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id              = required_param('id', PARAM_INT);                 // Course Module ID
$action          = optional_param('action', '', PARAM_ALPHA);
$userids         = optional_param_array('userid', array(), PARAM_INT); // array of attempt ids for delete action
$error           = optional_param('error', '', PARAM_INT);

$url = new moodle_url('/mod/moodecgrpmanagement/view.php', array('id'=>$id));
if ($action !== '') {
    $url->param('action', $action);
}

$PAGE->set_url($url);
$PAGE->requires->jquery();


if (! $cm = get_coursemodule_from_id('moodecgrpmanagement', $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
    print_error('coursemisconf');
}

require_login($course, false, $cm);

if (!$moodecgrpmanagement = moodecgrpmanagement_get_moodecgrpmanagement($cm->instance)) {
    print_error('invalidcoursemodule');
}

$moodecgrpmanagement_groups = moodecgrpmanagement_get_groups($moodecgrpmanagement);
$moodecgrpmanagement_users = array();

$strmoodecgrpmanagement = get_string('modulename', 'moodecgrpmanagement');
$strmoodecgrpmanagements = get_string('modulenameplural', 'moodecgrpmanagement');

if (!$context = context_module::instance($cm->id)) {
    print_error('badcontext');
}

$eventparams = array(
    'context' => $context,
    'objectid' => $moodecgrpmanagement->id
);

$current = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $USER);
if ($action == 'delmoodecgrpmanagement' and confirm_sesskey() and is_enrolled($context, NULL, 'mod/moodecgrpmanagement:choose') and $moodecgrpmanagement->allowupdate) {
    // user wants to delete his own choice:
    if ($current !== false) {
        if (groups_is_member($current->id, $USER->id)) {
            $currentgroup = $DB->get_record('groups', array('id' => $current->id), 'id,name', MUST_EXIST);
            groups_remove_member($current->id, $USER->id);
            $event = \mod_moodecgrpmanagement\event\choice_removed::create($eventparams);
            $event->add_record_snapshot('course_modules', $cm);
            $event->add_record_snapshot('course', $course);
            $event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
            $event->trigger();
        }
        $current = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $USER, FALSE, TRUE);
        // Update completion state
        $completion = new completion_info($course);
        if ($completion->is_enabled($cm) && $moodecgrpmanagement->completionsubmit) {
            $completion->update_state($cm, COMPLETION_INCOMPLETE);
        }
    }
}

$PAGE->set_title(format_string($moodecgrpmanagement->name));
$PAGE->set_heading($course->fullname);
$PAGE->requires->js('/mod/moodecgrpmanagement/lib/jquery.dynatable.js',true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/mod/moodecgrpmanagement/javascript.js'),true);
/// Mark as viewed
$completion=new completion_info($course);
$completion->set_module_viewed($cm);

/// Submit any new data if there is any
if (data_submitted() && is_enrolled($context, NULL, 'mod/moodecgrpmanagement:choose') && confirm_sesskey()) {

    if ($moodecgrpmanagement->multipleenrollmentspossible == 1) {


        $number_of_groups = optional_param('number_of_groups', '', PARAM_INT);

        $incorrectEnrollementKey = false;

        for ($i = 0; $i < $number_of_groups; $i++) {
            $answer_value = optional_param('answer_' . $i, '', PARAM_INT);
            if ($answer_value != '') {
                $selected_option = $DB->get_record('moodecgrpmanagement_options', array('id' =>$answer_value));
                $selected_option->enrollementkey = null ;
                if (!groups_is_member($selected_option->groupid, $USER->id)) {
                    $enrollementkey = optional_param('enrollementKeyKey'.$answer_value, '', PARAM_TEXT);
                    
                    if ($moodecgrpmanagement->privategroupspossible == 1) {

                        // select la bonne clé
                        $param = array($selected_option->groupid);
                        $query = "SELECT enrolmentkey FROM {groups} WHERE id= ?";
                        $enrolmentkey = $DB->get_field_sql($query,$param);

                        if(isset($enrolmentkey)) {
                            $selected_option->enrollementkey = $enrolmentkey;
                        }
                        if (!empty($selected_option->enrollementkey)) {
                            if ($enrollementkey != $selected_option->enrollementkey) {
                                $incorrectEnrollementKey = true;
                                continue;
                            }
                        }
                    }
                }

                moodecgrpmanagement_user_submit_response($answer_value, $moodecgrpmanagement, $USER->id, $course, $cm);
            } else {
                $answer_value_group_id = optional_param('answer_'.$i.'_groupid', '', PARAM_INT);
                if (groups_is_member($answer_value_group_id, $USER->id)) {
                    $answer_value_group = $DB->get_record('groups', array('id' => $answer_value_group_id), 'id,name', MUST_EXIST);
                    groups_remove_member($answer_value_group_id, $USER->id);
                    $event = \mod_moodecgrpmanagement\event\choice_removed::create($eventparams);
                    $event->add_record_snapshot('course_modules', $cm);
                    $event->add_record_snapshot('course', $course);
                    $event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
                    $event->trigger();
                }
            }
        }

        if($incorrectEnrollementKey) {
            redirect("view.php?id=$cm->id&error=1");
        }

    } else { // multipleenrollmentspossible != 1

        $timenow = time();
        if (has_capability('mod/moodecgrpmanagement:deleteresponses', $context)) {
            if ($action == 'delete') { //some responses need to be deleted
                moodecgrpmanagement_delete_responses($userids, $moodecgrpmanagement, $cm, $course); //delete responses.
                redirect("view.php?id=$cm->id");
            }
        }

        $answer = optional_param('answer', '', PARAM_INT);

        if (empty($answer)) {
            redirect("view.php?id=$cm->id", get_string('mustchooseone', 'moodecgrpmanagement'));
        } else {
            $incorrectEnrollementKey = false ;
            $selected_option = $DB->get_record('moodecgrpmanagement_options', array('id' =>$answer));
            $selected_option->enrollementkey = null ;

            $enrollementkey = optional_param('enrollementKeyKey'.$answer, '', PARAM_TEXT);

            // select la bonne clé
            $param = array($selected_option->groupid);
            $query = "SELECT enrolmentkey FROM {groups} WHERE id= ?";
            $enrolementkey = $DB->get_field_sql($query,$param);

            if(isset($enrolementkey)) {
                $selected_option->enrollementkey = $enrolementkey;
            }


            if ($moodecgrpmanagement->privategroupspossible == 1) {
                if (!empty($selected_option->enrollementkey)) {
                    if ($enrollementkey != $selected_option->enrollementkey) {
                        $incorrectEnrollementKey = true;
                    }
                }
            }
            if($incorrectEnrollementKey) {
                redirect("view.php?id=$cm->id&error=1");
            }

            moodecgrpmanagement_user_submit_response($answer, $moodecgrpmanagement, $USER->id, $course, $cm);
        }
    }

    redirect("view.php?id=$cm->id", get_string('moodecgrpmanagementsaved', 'moodecgrpmanagement'));
} else {
    echo $OUTPUT->header();
}


/// Display the moodecgrpmanagement and possibly results


$event = \mod_moodecgrpmanagement\event\course_module_viewed::create($eventparams);
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('moodecgrpmanagement', $moodecgrpmanagement);
$event->trigger();


/// Check to see if groups are being used in this moodecgrpmanagement
$groupmode = groups_get_activity_groupmode($cm);

if ($groupmode) {
    groups_get_activity_group($cm, true);
    groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/moodecgrpmanagement/view.php?id='.$id);
}

$allresponses = moodecgrpmanagement_get_response_data($moodecgrpmanagement, $cm);   // Big function, approx 6 SQL calls per user


if (has_capability('mod/moodecgrpmanagement:readresponses', $context)) {
    moodecgrpmanagement_show_reportlink($moodecgrpmanagement, $allresponses, $cm);
}

echo '<div class="clearer"></div>';

if ($moodecgrpmanagement->intro) {
    echo $OUTPUT->box(format_module_intro('moodecgrpmanagement', $moodecgrpmanagement, $cm->id), 'generalbox', 'intro');
}

//if user has already made a selection, and they are not allowed to update it, show their selected answer.
if (isloggedin() && ($current !== false) ) {
    if ($moodecgrpmanagement->multipleenrollmentspossible == 1) {
        $currents = moodecgrpmanagement_get_user_answer($moodecgrpmanagement, $USER, TRUE);

        $names = array();
        foreach ($currents as $current) {
            $names[] = format_string($current->name);
        }
        $formatted_names = join(' '.get_string("and", "moodecgrpmanagement").' ', array_filter(array_merge(array(join(', ', array_slice($names, 0, -1))), array_slice($names, -1))));
        echo $OUTPUT->box('<h2>'.get_string("yourselection", "moodecgrpmanagement", userdate($moodecgrpmanagement->timeopen)).':</h2>'.$formatted_names, 'generalbox', 'yourselection');

    } else {
        echo $OUTPUT->box('<h2>'.get_string("yourselection", "moodecgrpmanagement", userdate($moodecgrpmanagement->timeopen)).' :</h2>'.format_string($current->name), 'generalbox', 'yourselection');
    }
}

if(isset($error) && $error == 1) {
    echo $OUTPUT->box(get_string('incorrectEnrollementKey', 'moodecgrpmanagement'), 'generalbox enrollementKey enrollementKeyError errormessage');
}

if ($moodecgrpmanagement->freezegroups == 1 || (!empty($moodecgrpmanagement->freezegroupsaftertime) && time() >= $moodecgrpmanagement->freezegroupsaftertime)) {
    if (!empty($moodecgrpmanagement->freezegroupsaftertime) && time() >= $moodecgrpmanagement->freezegroupsaftertime) {
        $freezeTime = gmdate("Y-m-d H:i:s", $moodecgrpmanagement->freezegroupsaftertime);
        echo $OUTPUT->box(get_string("groupsfrozenaftertime", "moodecgrpmanagement").' '.$freezeTime, 'generalbox', 'frozenlabel');
    } else {
        echo $OUTPUT->box(get_string("groupsfrozen", "moodecgrpmanagement"), 'generalbox', 'frozenlabel');
    }
}

/// Print the form
$moodecgrpmanagementopen = true;
$timenow = time();
if ($moodecgrpmanagement->timeclose !=0) {
    if ($moodecgrpmanagement->timeopen > $timenow ) {
        echo $OUTPUT->box(get_string("notopenyet", "moodecgrpmanagement", userdate($moodecgrpmanagement->timeopen)), "generalbox notopenyet");
        echo $OUTPUT->footer();
        exit;
    } else if ($timenow > $moodecgrpmanagement->timeclose) {
        echo $OUTPUT->box(get_string("expired", "moodecgrpmanagement", userdate($moodecgrpmanagement->timeclose)), "generalbox expired");
        $moodecgrpmanagementopen = false;
    }
}

$options = moodecgrpmanagement_prepare_options($moodecgrpmanagement, $USER, $cm, $allresponses);
$renderer = $PAGE->get_renderer('mod_moodecgrpmanagement');
if ( (!$current or $moodecgrpmanagement->allowupdate) and $moodecgrpmanagementopen and is_enrolled($context, NULL, 'mod/moodecgrpmanagement:choose')) {
// They haven't made their moodecgrpmanagement yet or updates allowed and moodecgrpmanagement is open

    echo $renderer->display_options($options, $cm->id, $moodecgrpmanagement->display, $moodecgrpmanagement->publish, $moodecgrpmanagement->limitmaxusersingroups, $moodecgrpmanagement->showresults, $current, $moodecgrpmanagementopen, false, $moodecgrpmanagement->multipleenrollmentspossible);
} else {
    // form can not be updated
    echo $renderer->display_options($options, $cm->id, $moodecgrpmanagement->display, $moodecgrpmanagement->publish, $moodecgrpmanagement->limitmaxusersingroups, $moodecgrpmanagement->showresults, $current, $moodecgrpmanagementopen, true, $moodecgrpmanagement->multipleenrollmentspossible);
}
$moodecgrpmanagementformshown = true;

$sitecontext = context_system::instance();

if (isguestuser()) {
    // Guest account
    echo $OUTPUT->confirm(get_string('noguestchoose', 'moodecgrpmanagement').'<br /><br />'.get_string('liketologin'),
                    get_login_url(), new moodle_url('/course/view.php', array('id'=>$course->id)));
} else if (!is_enrolled($context)) {
    // Only people enrolled can make a moodecgrpmanagement
    $SESSION->wantsurl = $FULLME;
    $SESSION->enrolcancel = (!empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '';

    $coursecontext = context_course::instance($course->id);
    $courseshortname = format_string($course->shortname, true, array('context' => $coursecontext));

    echo $OUTPUT->box_start('generalbox', 'notice');
    echo '<p align="center">'. get_string('notenrolledchoose', 'moodecgrpmanagement') .'</p>';
    echo $OUTPUT->container_start('continuebutton');
    echo $OUTPUT->single_button(new moodle_url('/enrol/index.php?', array('id'=>$course->id)), get_string('enrolme', 'core_enrol', $courseshortname));
    echo $OUTPUT->container_end();
    echo $OUTPUT->box_end();

}

// print the results at the bottom of the screen
if ( $moodecgrpmanagement->showresults == moodecgrpmanagement_SHOWRESULTS_ALWAYS or
    ($moodecgrpmanagement->showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER and $current) or
    ($moodecgrpmanagement->showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE and !$moodecgrpmanagementopen)) {
}
else if ($moodecgrpmanagement->showresults == moodecgrpmanagement_SHOWRESULTS_NOT) {
    echo $OUTPUT->box(get_string('neverresultsviewable', 'moodecgrpmanagement'));
}
else if ($moodecgrpmanagement->showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_ANSWER && !$current) {
    echo $OUTPUT->box(get_string('afterresultsviewable', 'moodecgrpmanagement'));
}
else if ($moodecgrpmanagement->showresults == moodecgrpmanagement_SHOWRESULTS_AFTER_CLOSE and $moodecgrpmanagementopen) {
    echo $OUTPUT->box(get_string('notyetresultsviewable', 'moodecgrpmanagement'));
}
else if (!$moodecgrpmanagementformshown) {
    echo $OUTPUT->box(get_string('noresultsviewable', 'moodecgrpmanagement'));
}

echo $OUTPUT->footer();

